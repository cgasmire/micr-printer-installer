﻿namespace InstallationUtility
{
    partial class installTypeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(installTypeForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.headerDescriptionLabel = new System.Windows.Forms.Label();
            this.headerLabel = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.fullInstallationRadioButton = new System.Windows.Forms.RadioButton();
            this.existingQueueRadioButton = new System.Windows.Forms.RadioButton();
            this.ttfOnlyRadioButton = new System.Windows.Forms.RadioButton();
            this.fullInstallationLabel = new System.Windows.Forms.Label();
            this.existingQueueLabel = new System.Windows.Forms.Label();
            this.ttfOnlyLabel = new System.Windows.Forms.Label();
            this.versionLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.headerDescriptionLabel);
            this.groupBox1.Controls.Add(this.headerLabel);
            this.groupBox1.Location = new System.Drawing.Point(-1, -6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 59);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(347, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // headerDescriptionLabel
            // 
            this.headerDescriptionLabel.AutoSize = true;
            this.headerDescriptionLabel.Location = new System.Drawing.Point(55, 36);
            this.headerDescriptionLabel.Name = "headerDescriptionLabel";
            this.headerDescriptionLabel.Size = new System.Drawing.Size(231, 13);
            this.headerDescriptionLabel.TabIndex = 2;
            this.headerDescriptionLabel.Text = "Please choose the type of installation you want.";
            // 
            // headerLabel
            // 
            this.headerLabel.AutoSize = true;
            this.headerLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.headerLabel.Location = new System.Drawing.Point(42, 19);
            this.headerLabel.Name = "headerLabel";
            this.headerLabel.Size = new System.Drawing.Size(101, 13);
            this.headerLabel.TabIndex = 1;
            this.headerLabel.Text = "Installation Type";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.versionLabel);
            this.groupBox2.Controls.Add(this.cancelButton);
            this.groupBox2.Controls.Add(this.nextButton);
            this.groupBox2.Controls.Add(this.backButton);
            this.groupBox2.Location = new System.Drawing.Point(-11, 290);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(514, 68);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(414, 19);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 5;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(322, 19);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 4;
            this.nextButton.Text = "&Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(245, 19);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 3;
            this.backButton.Text = "&Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // fullInstallationRadioButton
            // 
            this.fullInstallationRadioButton.AutoSize = true;
            this.fullInstallationRadioButton.Checked = true;
            this.fullInstallationRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fullInstallationRadioButton.Location = new System.Drawing.Point(44, 90);
            this.fullInstallationRadioButton.Name = "fullInstallationRadioButton";
            this.fullInstallationRadioButton.Size = new System.Drawing.Size(110, 17);
            this.fullInstallationRadioButton.TabIndex = 0;
            this.fullInstallationRadioButton.TabStop = true;
            this.fullInstallationRadioButton.Text = "Full installation";
            this.fullInstallationRadioButton.UseVisualStyleBackColor = true;
            // 
            // existingQueueRadioButton
            // 
            this.existingQueueRadioButton.AutoSize = true;
            this.existingQueueRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.existingQueueRadioButton.Location = new System.Drawing.Point(44, 152);
            this.existingQueueRadioButton.Name = "existingQueueRadioButton";
            this.existingQueueRadioButton.Size = new System.Drawing.Size(137, 17);
            this.existingQueueRadioButton.TabIndex = 1;
            this.existingQueueRadioButton.Text = "Existing print queue";
            this.existingQueueRadioButton.UseVisualStyleBackColor = true;
            // 
            // ttfOnlyRadioButton
            // 
            this.ttfOnlyRadioButton.AutoSize = true;
            this.ttfOnlyRadioButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ttfOnlyRadioButton.Location = new System.Drawing.Point(44, 216);
            this.ttfOnlyRadioButton.Name = "ttfOnlyRadioButton";
            this.ttfOnlyRadioButton.Size = new System.Drawing.Size(138, 17);
            this.ttfOnlyRadioButton.TabIndex = 2;
            this.ttfOnlyRadioButton.Text = "TrueType fonts only";
            this.ttfOnlyRadioButton.UseVisualStyleBackColor = true;
            // 
            // fullInstallationLabel
            // 
            this.fullInstallationLabel.AutoSize = true;
            this.fullInstallationLabel.Location = new System.Drawing.Point(60, 110);
            this.fullInstallationLabel.Name = "fullInstallationLabel";
            this.fullInstallationLabel.Size = new System.Drawing.Size(378, 26);
            this.fullInstallationLabel.TabIndex = 3;
            this.fullInstallationLabel.Text = "Creates a new print queue using the HP Universal Printing PCL 5 driver, \r\nloads f" +
    "ont configurations to the queue, and installs the requred TrueType fonts.";
            // 
            // existingQueueLabel
            // 
            this.existingQueueLabel.AutoSize = true;
            this.existingQueueLabel.Location = new System.Drawing.Point(60, 172);
            this.existingQueueLabel.Name = "existingQueueLabel";
            this.existingQueueLabel.Size = new System.Drawing.Size(360, 26);
            this.existingQueueLabel.TabIndex = 4;
            this.existingQueueLabel.Text = "Loads font configurations to existing print queues that are using the \r\nHP Univer" +
    "sal Printing PCL 5 driver and installs the required TrueType fonts.";
            // 
            // ttfOnlyLabel
            // 
            this.ttfOnlyLabel.AutoSize = true;
            this.ttfOnlyLabel.Location = new System.Drawing.Point(60, 236);
            this.ttfOnlyLabel.Name = "ttfOnlyLabel";
            this.ttfOnlyLabel.Size = new System.Drawing.Size(334, 13);
            this.ttfOnlyLabel.TabIndex = 5;
            this.ttfOnlyLabel.Text = "Installs TrueType fonts but does not do any print queue configuration.";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(18, 45);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(31, 9);
            this.versionLabel.TabIndex = 8;
            this.versionLabel.Text = "Version";
            // 
            // installTypeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(490, 350);
            this.Controls.Add(this.ttfOnlyLabel);
            this.Controls.Add(this.existingQueueLabel);
            this.Controls.Add(this.fullInstallationLabel);
            this.Controls.Add(this.ttfOnlyRadioButton);
            this.Controls.Add(this.existingQueueRadioButton);
            this.Controls.Add(this.fullInstallationRadioButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "installTypeForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MICR Printer Installer";
            this.Load += new System.EventHandler(this.installType_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label headerDescriptionLabel;
        private System.Windows.Forms.Label headerLabel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.RadioButton fullInstallationRadioButton;
        private System.Windows.Forms.RadioButton existingQueueRadioButton;
        private System.Windows.Forms.RadioButton ttfOnlyRadioButton;
        private System.Windows.Forms.Label fullInstallationLabel;
        private System.Windows.Forms.Label existingQueueLabel;
        private System.Windows.Forms.Label ttfOnlyLabel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label versionLabel;
    }
}

