﻿namespace InstallationUtility
{
    partial class queueSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(queueSelectionForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.selectAllTestPageCheckBox = new System.Windows.Forms.CheckBox();
            this.versionLabel = new System.Windows.Forms.Label();
            this.cancelButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.printerDataGridView = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.printerSelectionToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.toolTipPlaceHolder = new System.Windows.Forms.Label();
            this.checkBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.printQueueColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.updCheckColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.testPageColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.borderColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printerDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(-1, -6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(557, 59);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(402, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(70, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(287, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Please select the print queue(s) you would like to configure.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(57, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Print Queue Selection";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.selectAllTestPageCheckBox);
            this.groupBox2.Controls.Add(this.versionLabel);
            this.groupBox2.Controls.Add(this.cancelButton);
            this.groupBox2.Controls.Add(this.backButton);
            this.groupBox2.Controls.Add(this.nextButton);
            this.groupBox2.Location = new System.Drawing.Point(-11, 290);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(579, 68);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // selectAllTestPageCheckBox
            // 
            this.selectAllTestPageCheckBox.AutoSize = true;
            this.selectAllTestPageCheckBox.Location = new System.Drawing.Point(70, 21);
            this.selectAllTestPageCheckBox.Name = "selectAllTestPageCheckBox";
            this.selectAllTestPageCheckBox.Size = new System.Drawing.Size(202, 17);
            this.selectAllTestPageCheckBox.TabIndex = 10;
            this.selectAllTestPageCheckBox.Text = "Print test page for all selected printers";
            this.selectAllTestPageCheckBox.UseVisualStyleBackColor = true;
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(18, 45);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(31, 9);
            this.versionLabel.TabIndex = 9;
            this.versionLabel.Text = "Version";
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(468, 19);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(300, 19);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "&Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(377, 19);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 7;
            this.nextButton.Text = "&Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // printerDataGridView
            // 
            this.printerDataGridView.AllowUserToAddRows = false;
            this.printerDataGridView.AllowUserToDeleteRows = false;
            this.printerDataGridView.AllowUserToResizeColumns = false;
            this.printerDataGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.printerDataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.printerDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.printerDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.checkBoxColumn,
            this.printQueueColumn,
            this.updCheckColumn,
            this.testPageColumn,
            this.borderColumn});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.printerDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.printerDataGridView.EnableHeadersVisualStyles = false;
            this.printerDataGridView.Location = new System.Drawing.Point(44, 76);
            this.printerDataGridView.Name = "printerDataGridView";
            this.printerDataGridView.RowHeadersVisible = false;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.printerDataGridView.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.printerDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.printerDataGridView.Size = new System.Drawing.Size(458, 178);
            this.printerDataGridView.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(72, 263);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(395, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Note: It is advised you only select queues using the recommended HP Universal \r\nP" +
    "rinting PCL 5 driver. Other PCL 5 drivers have not been tested and may not work." +
    "";
            // 
            // printerSelectionToolTip
            // 
            this.printerSelectionToolTip.AutoPopDelay = 5000;
            this.printerSelectionToolTip.InitialDelay = 32767;
            this.printerSelectionToolTip.IsBalloon = true;
            this.printerSelectionToolTip.ReshowDelay = 100;
            this.printerSelectionToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.printerSelectionToolTip.ToolTipTitle = "Selection Required";
            // 
            // toolTipPlaceHolder
            // 
            this.toolTipPlaceHolder.AutoSize = true;
            this.toolTipPlaceHolder.Location = new System.Drawing.Point(268, 115);
            this.toolTipPlaceHolder.Name = "toolTipPlaceHolder";
            this.toolTipPlaceHolder.Size = new System.Drawing.Size(0, 13);
            this.toolTipPlaceHolder.TabIndex = 5;
            // 
            // checkBoxColumn
            // 
            this.checkBoxColumn.HeaderText = "";
            this.checkBoxColumn.Name = "checkBoxColumn";
            this.checkBoxColumn.Width = 30;
            // 
            // printQueueColumn
            // 
            this.printQueueColumn.HeaderText = "Print Queue";
            this.printQueueColumn.Name = "printQueueColumn";
            this.printQueueColumn.ReadOnly = true;
            this.printQueueColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.printQueueColumn.Width = 333;
            // 
            // updCheckColumn
            // 
            this.updCheckColumn.HeaderText = "UPD PCL 5";
            this.updCheckColumn.Name = "updCheckColumn";
            this.updCheckColumn.ReadOnly = true;
            this.updCheckColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.updCheckColumn.Width = 75;
            // 
            // testPageColumn
            // 
            this.testPageColumn.HeaderText = "Test Page";
            this.testPageColumn.Name = "testPageColumn";
            this.testPageColumn.ReadOnly = true;
            this.testPageColumn.Visible = false;
            this.testPageColumn.Width = 63;
            // 
            // borderColumn
            // 
            this.borderColumn.HeaderText = "";
            this.borderColumn.Name = "borderColumn";
            this.borderColumn.ReadOnly = true;
            this.borderColumn.Width = 20;
            // 
            // queueSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(545, 350);
            this.Controls.Add(this.toolTipPlaceHolder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.printerDataGridView);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "queueSelectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MICR Printer Installer";
            this.Load += new System.EventHandler(this.queueSelectionForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.printerDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.DataGridView printerDataGridView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ToolTip printerSelectionToolTip;
        private System.Windows.Forms.Label toolTipPlaceHolder;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.CheckBox selectAllTestPageCheckBox;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn printQueueColumn;
        private System.Windows.Forms.DataGridViewImageColumn updCheckColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn testPageColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn borderColumn;
        //private System.Windows.Forms.ScrollViewer.VerticalScrollBarVisibility="True
    }
}

