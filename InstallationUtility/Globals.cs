﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstallationUtility
{
    class Globals
    {
        public static List<string> checkedRows = new List<string>();
        public static List<bool> checkedTestPages = new List<bool>();
        public static List<string> checkedFonts = new List<string>();
        public static String installationType = "Full Install";
        public static String systemType = "One";
        public static String printerName = "TROY MICR Printer";
        public static String printerPort = null;
        public static Queue<Action> loadingQueue = new Queue<Action>();
        public static String statusLabel = "Loading components...";
        public static bool ipIsValid = false;
        public static bool createNewPort = false;
        public static int installStatus = 0;
        public static bool cancelInstallFlag = false;
        public static bool deleteExistingFonts = false;
        public static bool deleteFontCache = false;
        public static String micrTestPath = "";
        public static String signatureTestPath = "";
    }
}
