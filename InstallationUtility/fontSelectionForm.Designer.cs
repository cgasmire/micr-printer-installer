﻿namespace InstallationUtility
{
    partial class fontSelectionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fontSelectionForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.placeholderLabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.internationalCheckBox = new System.Windows.Forms.CheckBox();
            this.domesticCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.barcodeCheckBox = new System.Windows.Forms.CheckBox();
            this.imaging12CheckBox = new System.Windows.Forms.CheckBox();
            this.imaging72checkBox = new System.Windows.Forms.CheckBox();
            this.noSelectionToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.versionLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(-1, -6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 59);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(347, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Please select the fonts you would like to install.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Font Selection";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.versionLabel);
            this.groupBox2.Controls.Add(this.cancelButton);
            this.groupBox2.Controls.Add(this.backButton);
            this.groupBox2.Controls.Add(this.nextButton);
            this.groupBox2.Location = new System.Drawing.Point(-11, 290);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(514, 68);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(414, 19);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(245, 19);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "&Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(322, 19);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 7;
            this.nextButton.Text = "&Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.placeholderLabel);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.internationalCheckBox);
            this.groupBox3.Controls.Add(this.domesticCheckBox);
            this.groupBox3.Location = new System.Drawing.Point(44, 66);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(405, 114);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "MICR";
            // 
            // placeholderLabel
            // 
            this.placeholderLabel.AutoSize = true;
            this.placeholderLabel.Location = new System.Drawing.Point(203, 77);
            this.placeholderLabel.Name = "placeholderLabel";
            this.placeholderLabel.Size = new System.Drawing.Size(0, 13);
            this.placeholderLabel.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 91);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(152, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "British, French, Italian, Spanish";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 41);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(362, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "E-13B, CMC-7, ECF, LCF, Micro, OCR-A, OCR-B, Rev Helv, SCF, Security, \r\nAuto-Prot" +
    "ect";
            // 
            // internationalCheckBox
            // 
            this.internationalCheckBox.AutoSize = true;
            this.internationalCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.internationalCheckBox.Location = new System.Drawing.Point(13, 73);
            this.internationalCheckBox.Name = "internationalCheckBox";
            this.internationalCheckBox.Size = new System.Drawing.Size(97, 17);
            this.internationalCheckBox.TabIndex = 1;
            this.internationalCheckBox.Text = "International";
            this.internationalCheckBox.UseVisualStyleBackColor = true;
            // 
            // domesticCheckBox
            // 
            this.domesticCheckBox.AutoSize = true;
            this.domesticCheckBox.Checked = true;
            this.domesticCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.domesticCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.domesticCheckBox.Location = new System.Drawing.Point(13, 20);
            this.domesticCheckBox.Name = "domesticCheckBox";
            this.domesticCheckBox.Size = new System.Drawing.Size(78, 17);
            this.domesticCheckBox.TabIndex = 0;
            this.domesticCheckBox.Text = "Domestic";
            this.domesticCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.barcodeCheckBox);
            this.groupBox4.Controls.Add(this.imaging12CheckBox);
            this.groupBox4.Controls.Add(this.imaging72checkBox);
            this.groupBox4.Location = new System.Drawing.Point(44, 186);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(405, 90);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Imaging/Barcode";
            // 
            // barcodeCheckBox
            // 
            this.barcodeCheckBox.AutoSize = true;
            this.barcodeCheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barcodeCheckBox.Location = new System.Drawing.Point(13, 66);
            this.barcodeCheckBox.Name = "barcodeCheckBox";
            this.barcodeCheckBox.Size = new System.Drawing.Size(73, 17);
            this.barcodeCheckBox.TabIndex = 2;
            this.barcodeCheckBox.Text = "Barcode";
            this.barcodeCheckBox.UseVisualStyleBackColor = true;
            // 
            // imaging12CheckBox
            // 
            this.imaging12CheckBox.AutoSize = true;
            this.imaging12CheckBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imaging12CheckBox.Location = new System.Drawing.Point(13, 43);
            this.imaging12CheckBox.Name = "imaging12CheckBox";
            this.imaging12CheckBox.Size = new System.Drawing.Size(99, 17);
            this.imaging12CheckBox.TabIndex = 1;
            this.imaging12CheckBox.Text = "12pt Imaging";
            this.imaging12CheckBox.UseVisualStyleBackColor = true;
            // 
            // imaging72checkBox
            // 
            this.imaging72checkBox.AutoSize = true;
            this.imaging72checkBox.Checked = true;
            this.imaging72checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.imaging72checkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.imaging72checkBox.Location = new System.Drawing.Point(13, 20);
            this.imaging72checkBox.Name = "imaging72checkBox";
            this.imaging72checkBox.Size = new System.Drawing.Size(99, 17);
            this.imaging72checkBox.TabIndex = 0;
            this.imaging72checkBox.Text = "72pt Imaging";
            this.imaging72checkBox.UseVisualStyleBackColor = true;
            // 
            // noSelectionToolTip
            // 
            this.noSelectionToolTip.AutoPopDelay = 5000;
            this.noSelectionToolTip.InitialDelay = 32767;
            this.noSelectionToolTip.IsBalloon = true;
            this.noSelectionToolTip.ReshowDelay = 100;
            this.noSelectionToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.noSelectionToolTip.ToolTipTitle = "Selection Required";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(18, 45);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(31, 9);
            this.versionLabel.TabIndex = 9;
            this.versionLabel.Text = "Version";
            // 
            // fontSelectionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(490, 350);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "fontSelectionForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MICR Printer Installer";
            this.Load += new System.EventHandler(this.fontSelectionForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox internationalCheckBox;
        private System.Windows.Forms.CheckBox domesticCheckBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox barcodeCheckBox;
        private System.Windows.Forms.CheckBox imaging12CheckBox;
        private System.Windows.Forms.CheckBox imaging72checkBox;
        private System.Windows.Forms.ToolTip noSelectionToolTip;
        private System.Windows.Forms.Label placeholderLabel;
        private System.Windows.Forms.Label versionLabel;
    }
}

