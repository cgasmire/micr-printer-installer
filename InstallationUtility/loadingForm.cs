﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;

//Errors generated from this section will have an Event ID in the range of 700-799

namespace InstallationUtility
{
    public partial class loadingForm : Form
    {
        public loadingForm()
        {
            InitializeComponent();
        }

        void unpackingStatus()
        {
            Globals.statusLabel = "Unpacking files...";
            Globals.installStatus = 1;
        }

        void installingDriverStatus()
        {
            Globals.statusLabel = "Installing driver and creating queue...";
            Globals.installStatus = 2;
        }

        void updatingRegistryStatus()
        {
            Globals.statusLabel = "Updating registry entries...";
            Globals.installStatus = 3;
        }

        void rebuildingFontCacheStatus()
        {
            Globals.statusLabel = "Rebuilding font cache...";
            Globals.installStatus = 4;
        }

        void deletingFontsStatus()
        {
            Globals.statusLabel = "Deleting fonts...";
            Globals.installStatus = 5;
        }

        void installingFontsStatus()
        {
            Globals.statusLabel = "Installing fonts...";
            Globals.installStatus = 6;
        }

        void completeStatus()
        {
            Globals.statusLabel = "Installation complete";
            Globals.installStatus = 7;
        }

        private async Task runLoadingQueueAsync()
        {
            //Loop through all queued items, running each to completion before moving to the next
            foreach (var action in Globals.loadingQueue)
            {
                var t = Task.Run(() =>
                {
                    action();
                });
                await t;

                //Update statusLabel and progressBar values after each action completes
                statusLabel.Text = Globals.statusLabel;
                progressBar.Value += (progressBar.Maximum - progressBar.Minimum) / Globals.loadingQueue.Count;
            }
        }

        private void loadingForm_Load(object sender, EventArgs e)
        {
            versionLabel.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }

        private async void loadingForm_Shown(object sender, EventArgs e)
        {
            //Queue actions based on installationType
            switch (Globals.installationType)
            {
                case "Full Install":

                    Globals.loadingQueue.Enqueue(unpackingStatus);
                    Globals.loadingQueue.Enqueue(Functions.unpackFiles);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    Globals.loadingQueue.Enqueue(installingDriverStatus);

                    if (Globals.createNewPort)
                    {
                        Globals.loadingQueue.Enqueue(Functions.installPort);
                    }

                    Globals.loadingQueue.Enqueue(Functions.installDriver);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);

                    if (Globals.deleteExistingFonts)
                    {
                        Globals.loadingQueue.Enqueue(deletingFontsStatus);
                        Globals.loadingQueue.Enqueue(Functions.deleteFonts);
                        Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    }

                    if (Globals.deleteFontCache)
                    {
                        Globals.loadingQueue.Enqueue(rebuildingFontCacheStatus);
                        Globals.loadingQueue.Enqueue(Functions.rebuildFontCache);
                        Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    }

                    Globals.loadingQueue.Enqueue(installingFontsStatus);
                    Globals.loadingQueue.Enqueue(Functions.installTTFs);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    Globals.loadingQueue.Enqueue(updatingRegistryStatus);
                    Globals.loadingQueue.Enqueue(Functions.setRegistryKeys);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    Globals.loadingQueue.Enqueue(completeStatus);

                    break;

                case "Existing Queue":

                    Globals.loadingQueue.Enqueue(unpackingStatus);
                    Globals.loadingQueue.Enqueue(Functions.unpackFiles);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);

                    if (Globals.deleteExistingFonts)
                    {
                        Globals.loadingQueue.Enqueue(deletingFontsStatus);
                        Globals.loadingQueue.Enqueue(Functions.deleteFonts);
                        Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    }

                    if (Globals.deleteFontCache)
                    {
                        Globals.loadingQueue.Enqueue(rebuildingFontCacheStatus);
                        Globals.loadingQueue.Enqueue(Functions.rebuildFontCache);
                        Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    }

                    Globals.loadingQueue.Enqueue(installingFontsStatus);
                    Globals.loadingQueue.Enqueue(Functions.installTTFs);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    Globals.loadingQueue.Enqueue(updatingRegistryStatus);
                    Globals.loadingQueue.Enqueue(Functions.setRegistryKeys);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    Globals.loadingQueue.Enqueue(completeStatus);

                    break;

                case "TTFs Only":

                    Globals.loadingQueue.Enqueue(unpackingStatus);
                    Globals.loadingQueue.Enqueue(Functions.unpackFiles);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);

                    if (Globals.deleteExistingFonts)
                    {
                        Globals.loadingQueue.Enqueue(deletingFontsStatus);
                        Globals.loadingQueue.Enqueue(Functions.deleteFonts);
                        Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    }

                    if (Globals.deleteFontCache)
                    {
                        Globals.loadingQueue.Enqueue(rebuildingFontCacheStatus);
                        Globals.loadingQueue.Enqueue(Functions.rebuildFontCache);
                        Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    }

                    Globals.loadingQueue.Enqueue(installingFontsStatus);
                    Globals.loadingQueue.Enqueue(Functions.installTTFs);
                    Globals.loadingQueue.Enqueue(Functions.cancelInstallCheck);
                    Globals.loadingQueue.Enqueue(completeStatus);

                    break;

                case "Full Multi":
                    break;
            }

            ////Loop through all queued items, running each to completion before moving to the next
            //foreach (var action in Globals.loadingQueue)
            //{
            //    var t = Task.Run(() =>
            //    {
            //        action();
            //    });
            //    t.Wait();

            //    //Update statusLabel and progressBar values after each action completes
            //    statusLabel.Text = Globals.statusLabel;
            //    progressBar.Value += (progressBar.Maximum - progressBar.Minimum) / Globals.loadingQueue.Count;
            //}

            await runLoadingQueueAsync();

            //When all items are finished max out the progressBar and set the label status to "Finished"
            nextButton.Enabled = true;
            backButton.Enabled = false;
            cancelButton.Enabled = false;
            progressBar.Value = progressBar.Maximum;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            fontSelectionForm fontSelectionForm = new fontSelectionForm();
            fontSelectionForm.Show();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            this.Close();
            completedForm completedForm = new completedForm();
            completedForm.Show();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Globals.cancelInstallFlag = true;
            Console.WriteLine("Cancel = true");
            statusLabel.Text = "Preparing to cancel. Please wait...";
        }
    }
}
