﻿using System;
using System.Linq;
using System.Windows.Forms;

//Errors generated from this section will have an Event ID in the range of 600-699

namespace InstallationUtility
{
    public partial class fontSelectionForm : Form
    {
        public fontSelectionForm()
        {
            InitializeComponent();
        }

        private void fontSelectionForm_Load(object sender, EventArgs e)
        {
            versionLabel.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            noSelectionToolTip.SetToolTip(placeholderLabel, "Selection Required");
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            //Initialize variables
            Globals.installationType = "Full Install";
            Globals.systemType = "One";

            this.Close();
            installTypeForm installTypeForm = new installTypeForm();
            installTypeForm.Show();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            //Add each selection to the checkedFonts list
            if (domesticCheckBox.Checked == true)
            {
                Globals.checkedFonts.Add("Domestic");
            }
            if (internationalCheckBox.Checked == true)
            {
                Globals.checkedFonts.Add("International");
            }
            if (imaging72checkBox.Checked == true)
            {
                Globals.checkedFonts.Add("72pt");
            }
            if (imaging12CheckBox.Checked == true)
            {
                Globals.checkedFonts.Add("12pt");
            }
            if (barcodeCheckBox.Checked == true)
            {
                Globals.checkedFonts.Add("Barcode");
            }

            //If there are no selections warn the user
            if (!Globals.checkedFonts.Any())
            {
                noSelectionToolTip.Hide(placeholderLabel);
                noSelectionToolTip.Show(@"Please select at least one set of fonts to install.", placeholderLabel, 5000);
            }

            //If there are selections, move to the next form
            else
            {
                this.Hide();
                confirmationForm confirmationForm = new confirmationForm();
                confirmationForm.Show();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
