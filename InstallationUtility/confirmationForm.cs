﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstallationUtility
{
    public partial class confirmationForm : Form
    {
        bool advancedClicked = false;
        public confirmationForm()
        {
            InitializeComponent();
        }

        private void confirmationForm_Load(object sender, EventArgs e)
        {
            installationTypeLabel.Text = Globals.installationType;
            fontsLabel.Text = "";

            switch (Globals.installationType)
            {
                case "Full Install":

                    numberOfPrintersLabel.Text = "1";
                    label8.Visible = true;
                    label7.Visible = true;
                    printerNameLabel.Visible = true;
                    portLabel.Visible = true;
                    printerNameLabel.Text = Globals.printerName;
                    portLabel.Text = Globals.printerPort;

                    break;

                case "Existing Queue":

                    numberOfPrintersLabel.Text = Globals.checkedRows.Count.ToString();
                    label8.Visible = false;
                    label7.Visible = false;
                    printerNameLabel.Visible = false;
                    portLabel.Visible = false;

                    break;

                case "TTFs Only":

                    numberOfPrintersLabel.Text = "0";
                    label8.Visible = false;
                    label7.Visible = false;
                    printerNameLabel.Visible = false;
                    portLabel.Visible = false;

                    break;
            }

            if (Globals.checkedFonts.Contains("Domestic"))
            {
                fontsLabel.Text += "Domestic\r\n";
            }

            if (Globals.checkedFonts.Contains("International"))
            {
                fontsLabel.Text += "International\r\n";
            }

            if (Globals.checkedFonts.Contains("72pt"))
            {
                fontsLabel.Text += "72pt Imaging\r\n";
            }

            if (Globals.checkedFonts.Contains("12pt"))
            {
                fontsLabel.Text += "12pt Imaging\r\n";
            }

            if (Globals.checkedFonts.Contains("Barcode"))
            {
                fontsLabel.Text += "Barcode\r\n";
            }
        }

        private void advancedLabel_Click(object sender, EventArgs e)
        {
            if (!advancedClicked)
            {
                advancedLabel.Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Bold);
                advancedClicked = true;
                deleteFontsCheckBox.Visible = true;
                deleteCacheCheckBox.Visible = true;
            }

            else
            {
                advancedLabel.Font = new Font(FontFamily.GenericSansSerif, 8, FontStyle.Regular);
                advancedClicked = false;
                deleteFontsCheckBox.Visible = false;
                deleteCacheCheckBox.Visible = false;
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            //Initialize variables
            Globals.installationType = "Full Installation";
            Globals.systemType = "One";

            this.Close();
            installTypeForm installTypeForm = new installTypeForm();
            installTypeForm.Show();
        }

        private void installButton_Click(object sender, EventArgs e)
        {
            Globals.deleteExistingFonts = deleteFontsCheckBox.Checked;
            Globals.deleteFontCache = deleteCacheCheckBox.Checked;

            Console.WriteLine(Globals.deleteExistingFonts);
            Console.WriteLine(Globals.deleteFontCache);

            this.Hide();
            loadingForm loadingForm = new loadingForm();
            loadingForm.Show();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
