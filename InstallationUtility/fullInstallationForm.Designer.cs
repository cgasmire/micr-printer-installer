﻿namespace InstallationUtility
{
    partial class fullInstallationForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fullInstallationForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.installMultipleButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.nextButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.existingPortRadioButton = new System.Windows.Forms.RadioButton();
            this.newPortRadioButton = new System.Windows.Forms.RadioButton();
            this.existingPortComboBox = new System.Windows.Forms.ComboBox();
            this.tcpipToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.systemTypeCheckBox = new System.Windows.Forms.CheckBox();
            this.ipTextBox = new System.Windows.Forms.TextBox();
            this.ipErrorPictureBox = new System.Windows.Forms.PictureBox();
            this.existingPortToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.existingPortErrorPictureBox = new System.Windows.Forms.PictureBox();
            this.printerNameToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.printerNameTextBox = new System.Windows.Forms.TextBox();
            this.versionLabel = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ipErrorPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.existingPortErrorPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(-1, -6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(492, 59);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(347, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(138, 40);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(283, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Please provide details about the print queue to be created.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(42, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Printer Details";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.versionLabel);
            this.groupBox2.Controls.Add(this.installMultipleButton);
            this.groupBox2.Controls.Add(this.cancelButton);
            this.groupBox2.Controls.Add(this.backButton);
            this.groupBox2.Controls.Add(this.nextButton);
            this.groupBox2.Location = new System.Drawing.Point(-11, 290);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(514, 68);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            // 
            // installMultipleButton
            // 
            this.installMultipleButton.Enabled = false;
            this.installMultipleButton.Location = new System.Drawing.Point(46, 18);
            this.installMultipleButton.Name = "installMultipleButton";
            this.installMultipleButton.Size = new System.Drawing.Size(81, 23);
            this.installMultipleButton.TabIndex = 9;
            this.installMultipleButton.Text = "Install &Multiple";
            this.installMultipleButton.UseVisualStyleBackColor = true;
            this.installMultipleButton.Visible = false;
            this.installMultipleButton.Click += new System.EventHandler(this.installMultipleButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(414, 19);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 8;
            this.cancelButton.Text = "&Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(245, 19);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "&Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // nextButton
            // 
            this.nextButton.Location = new System.Drawing.Point(322, 19);
            this.nextButton.Name = "nextButton";
            this.nextButton.Size = new System.Drawing.Size(75, 23);
            this.nextButton.TabIndex = 7;
            this.nextButton.Text = "&Next";
            this.nextButton.UseVisualStyleBackColor = true;
            this.nextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(57, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Printer name:";
            // 
            // existingPortRadioButton
            // 
            this.existingPortRadioButton.AutoSize = true;
            this.existingPortRadioButton.Checked = true;
            this.existingPortRadioButton.Location = new System.Drawing.Point(60, 158);
            this.existingPortRadioButton.Name = "existingPortRadioButton";
            this.existingPortRadioButton.Size = new System.Drawing.Size(106, 17);
            this.existingPortRadioButton.TabIndex = 6;
            this.existingPortRadioButton.TabStop = true;
            this.existingPortRadioButton.Text = "Use existing port:";
            this.existingPortRadioButton.UseVisualStyleBackColor = true;
            // 
            // newPortRadioButton
            // 
            this.newPortRadioButton.AutoSize = true;
            this.newPortRadioButton.Location = new System.Drawing.Point(60, 197);
            this.newPortRadioButton.Name = "newPortRadioButton";
            this.newPortRadioButton.Size = new System.Drawing.Size(142, 17);
            this.newPortRadioButton.TabIndex = 7;
            this.newPortRadioButton.Text = "Create new TCP/IP port:";
            this.newPortRadioButton.UseVisualStyleBackColor = true;
            // 
            // existingPortComboBox
            // 
            this.existingPortComboBox.FormattingEnabled = true;
            this.existingPortComboBox.Location = new System.Drawing.Point(234, 158);
            this.existingPortComboBox.Name = "existingPortComboBox";
            this.existingPortComboBox.Size = new System.Drawing.Size(212, 21);
            this.existingPortComboBox.TabIndex = 9;
            // 
            // tcpipToolTip
            // 
            this.tcpipToolTip.AutoPopDelay = 5000;
            this.tcpipToolTip.InitialDelay = 32767;
            this.tcpipToolTip.IsBalloon = true;
            this.tcpipToolTip.ReshowDelay = 100;
            this.tcpipToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.tcpipToolTip.ToolTipTitle = "Required Format";
            // 
            // systemTypeCheckBox
            // 
            this.systemTypeCheckBox.AutoSize = true;
            this.systemTypeCheckBox.Location = new System.Drawing.Point(60, 246);
            this.systemTypeCheckBox.Name = "systemTypeCheckBox";
            this.systemTypeCheckBox.Size = new System.Drawing.Size(187, 17);
            this.systemTypeCheckBox.TabIndex = 9;
            this.systemTypeCheckBox.Text = "Load both 32-bit and 64-bit drivers";
            this.systemTypeCheckBox.UseVisualStyleBackColor = true;
            this.systemTypeCheckBox.Visible = false;
            // 
            // ipTextBox
            // 
            this.ipTextBox.Enabled = false;
            this.ipTextBox.Location = new System.Drawing.Point(234, 196);
            this.ipTextBox.MaxLength = 15;
            this.ipTextBox.Name = "ipTextBox";
            this.ipTextBox.Size = new System.Drawing.Size(212, 20);
            this.ipTextBox.TabIndex = 10;
            // 
            // ipErrorPictureBox
            // 
            this.ipErrorPictureBox.Image = global::InstallationUtility.Properties.Resources.X;
            this.ipErrorPictureBox.InitialImage = global::InstallationUtility.Properties.Resources.X;
            this.ipErrorPictureBox.Location = new System.Drawing.Point(452, 198);
            this.ipErrorPictureBox.Name = "ipErrorPictureBox";
            this.ipErrorPictureBox.Size = new System.Drawing.Size(18, 18);
            this.ipErrorPictureBox.TabIndex = 11;
            this.ipErrorPictureBox.TabStop = false;
            this.ipErrorPictureBox.Visible = false;
            // 
            // existingPortToolTip
            // 
            this.existingPortToolTip.AutoPopDelay = 5000;
            this.existingPortToolTip.InitialDelay = 32767;
            this.existingPortToolTip.IsBalloon = true;
            this.existingPortToolTip.ReshowDelay = 100;
            this.existingPortToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.existingPortToolTip.ToolTipTitle = "Select a port";
            // 
            // existingPortErrorPictureBox
            // 
            this.existingPortErrorPictureBox.Image = global::InstallationUtility.Properties.Resources.X;
            this.existingPortErrorPictureBox.InitialImage = global::InstallationUtility.Properties.Resources.X;
            this.existingPortErrorPictureBox.Location = new System.Drawing.Point(452, 159);
            this.existingPortErrorPictureBox.Name = "existingPortErrorPictureBox";
            this.existingPortErrorPictureBox.Size = new System.Drawing.Size(18, 18);
            this.existingPortErrorPictureBox.TabIndex = 12;
            this.existingPortErrorPictureBox.TabStop = false;
            this.existingPortErrorPictureBox.Visible = false;
            // 
            // printerNameToolTip
            // 
            this.printerNameToolTip.AutoPopDelay = 5000;
            this.printerNameToolTip.InitialDelay = 32767;
            this.printerNameToolTip.IsBalloon = true;
            this.printerNameToolTip.ReshowDelay = 100;
            this.printerNameToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Error;
            this.printerNameToolTip.ToolTipTitle = "Required Format";
            // 
            // printerNameTextBox
            // 
            this.printerNameTextBox.Location = new System.Drawing.Point(132, 107);
            this.printerNameTextBox.MaxLength = 50;
            this.printerNameTextBox.Name = "printerNameTextBox";
            this.printerNameTextBox.Size = new System.Drawing.Size(314, 20);
            this.printerNameTextBox.TabIndex = 14;
            this.printerNameTextBox.WordWrap = false;
            this.printerNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.printerNameTextBox_KeyPress);
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(18, 45);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(31, 9);
            this.versionLabel.TabIndex = 15;
            this.versionLabel.Text = "Version";
            // 
            // fullInstallationForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(490, 350);
            this.Controls.Add(this.printerNameTextBox);
            this.Controls.Add(this.existingPortErrorPictureBox);
            this.Controls.Add(this.ipErrorPictureBox);
            this.Controls.Add(this.ipTextBox);
            this.Controls.Add(this.systemTypeCheckBox);
            this.Controls.Add(this.existingPortComboBox);
            this.Controls.Add(this.newPortRadioButton);
            this.Controls.Add(this.existingPortRadioButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "fullInstallationForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MICR Printer Installer";
            this.Load += new System.EventHandler(this.fullInstallationForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ipErrorPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.existingPortErrorPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button nextButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton existingPortRadioButton;
        private System.Windows.Forms.RadioButton newPortRadioButton;
        private System.Windows.Forms.ComboBox existingPortComboBox;
        private System.Windows.Forms.ToolTip tcpipToolTip;
        private System.Windows.Forms.CheckBox systemTypeCheckBox;
        private System.Windows.Forms.Button installMultipleButton;
        private System.Windows.Forms.TextBox ipTextBox;
        private System.Windows.Forms.PictureBox ipErrorPictureBox;
        private System.Windows.Forms.ToolTip existingPortToolTip;
        private System.Windows.Forms.PictureBox existingPortErrorPictureBox;
        private System.Windows.Forms.ToolTip printerNameToolTip;
        private System.Windows.Forms.TextBox printerNameTextBox;
        private System.Windows.Forms.Label versionLabel;
    }
}

