﻿using System;
using System.Windows.Forms;

//Errors generated from this section will have an Event ID in the range of 300-399

namespace InstallationUtility
{
    public partial class installTypeForm : Form
    {

        public installTypeForm()
        {
            InitializeComponent();
        }

        private void installType_Load(object sender, EventArgs e)
        {
            versionLabel.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            //Initialize Global variables on form load for when users press back button on later forms
            Functions.initializeGlobals();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            agreementForm agreementForm = new agreementForm();
            agreementForm.Show();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            //Update Globals and open next form depending on radio button selection
            if (fullInstallationRadioButton.Checked == true)
            {
                Globals.installationType = "Full Install";
                this.Close();
                fullInstallationForm fullInstallationForm = new fullInstallationForm();
                fullInstallationForm.Show();
            }

            else if (existingQueueRadioButton.Checked == true)
            {
                Globals.installationType = "Existing Queue";
                this.Hide();
                queueSelectionForm queueSelectionForm = new queueSelectionForm();
                queueSelectionForm.Show();
            }

            else
            {
                Globals.installationType = "TTFs Only";
                this.Hide();
                fontSelectionForm fontSelectionForm = new fontSelectionForm();
                fontSelectionForm.Show();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
