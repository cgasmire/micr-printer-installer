﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;
using System.Printing;
using System.Drawing.Printing;

//Errors generated from this section will have an Event ID in the range of 800-899

namespace InstallationUtility
{
    public partial class completedForm : Form
    {
        public completedForm()
        {
            InitializeComponent();

            FormClosing += completedForm_Closing;
        }

        private void completedForm_Load(object sender, EventArgs e)
        {
            versionLabel.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            if (Globals.deleteExistingFonts || Globals.deleteFontCache)
            {
                rebootLabel.Visible = true;
            }

            else
            {
                rebootLabel.Visible = false;
            }

            //The test print button is being removed at the request of tech support due to issues where Word/Wordpad isn't 
            //loading the test page correctly and therefore is not allowing the print. The button visibility has been disabled in the form designer.

            //if (Globals.installationType == "TTFs Only" || !Globals.checkedFonts.Contains("Domestic") || !Globals.checkedFonts.Contains("72pt"))
            //{
            //    testPrintButton.Enabled = false;
            //}
        }

        private void completedForm_Closing(Object sender, FormClosingEventArgs e)
        {
            if (devicesAndPrintersCheckBox.Checked)
            {
                string controlpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "control.exe");
                Process.Start(controlpath, "/name Microsoft.DevicesAndPrinters");
            }
        }

        private void testPrintButton_Click(object sender, EventArgs e)
        {

            if (Globals.installationType == "Full Install")
            {
                if (Globals.checkedFonts.Contains("Domestic"))
                {
                    var a = Task.Run(() =>
                    {
                        try
                        {
                            Functions.printDocument(Globals.micrTestPath, Globals.printerName);
                        }

                        catch (Exception e1)
                        {
                            EventLog.WriteEntry("Troy MICR Printer Installer", e1.Message + " Trace:" + e1.StackTrace, EventLogEntryType.Error, 800);
                        }
                    });

                    a.Wait();

                }

                if (Globals.checkedFonts.Contains("72pt"))
                {
                    var a = Task.Run(() =>
                    {
                        try
                        {
                            Functions.printDocument(Globals.signatureTestPath, Globals.printerName);
                        }

                        catch (Exception e2)
                        {
                            EventLog.WriteEntry("Troy MICR Printer Installer", e2.Message + " Trace:" + e2.StackTrace, EventLogEntryType.Error, 801);
                        }
                    });

                    a.Wait();
                }
            }

            else if (Globals.installationType == "Existing Queue")
            {
                int listItem = 0;

                foreach (var printer in Globals.checkedRows)
                {
                    if (Globals.checkedTestPages[listItem])
                    {
                        if (Globals.checkedFonts.Contains("Domestic"))
                        {
                            var a = Task.Run(() =>
                            {
                                try
                                {
                                    Functions.printDocument(Globals.micrTestPath, printer);
                                }

                                catch (Exception e1)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e1.Message + " Trace:" + e1.StackTrace, EventLogEntryType.Error, 802);
                                }
                            });

                            a.Wait();
                        }

                        if (Globals.checkedFonts.Contains("72pt"))
                        {
                            var a = Task.Run(() =>
                            {
                                try
                                {
                                    Functions.printDocument(Globals.signatureTestPath, printer);
                                }

                                catch (Exception e2)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e2.Message + " Trace:" + e2.StackTrace, EventLogEntryType.Error, 803);
                                }
                            });

                            a.Wait();
                        }
                    }

                    //System.Threading.Thread.Sleep(1000);

                    listItem++;
                }
            }            
        }

        private void finishButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
