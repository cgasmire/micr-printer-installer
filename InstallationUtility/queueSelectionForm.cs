﻿using Microsoft.Win32;
using System;
using System.Drawing;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Windows.Forms;

//Errors generated from this section will have an Event ID in the range of 500-599

namespace InstallationUtility
{
    public partial class queueSelectionForm : Form
    {
        public queueSelectionForm()
        {
            InitializeComponent();

            printerDataGridView.Enter += printerDataGridView_Enter;
            selectAllTestPageCheckBox.CheckedChanged += selectAllTestPageCheckBox_CheckedChanged;
            printerDataGridView.CellValueChanged += printerDataGridView_CellValueChanged;
            printerDataGridView.CellMouseUp += printerDataGridView_CellMouseUp;
        }

        private void queueSelectionForm_Load(object sender, EventArgs e)
        {
            versionLabel.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            printerSelectionToolTip.SetToolTip(toolTipPlaceHolder, "Selection Required");
            //Counter variable indicating the current row in the loop
            int currentRow = 0;

            //Populate the printer table
            foreach (string printerString in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                //Console.WriteLine(printerString);                

                //Add a new row for each installed printer
                printerDataGridView.Rows.Add();
                printerDataGridView.Rows[currentRow].Cells[1].Value = printerString;

                //Change the checkbox column color so it appears editable
                printerDataGridView.Rows[currentRow].Cells[0].Style.BackColor = SystemColors.Control;

                //Check the registry for the driver each installed queue is using
                RegistryKey key = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Print\Printers\" + printerString);

                //This base if statement was added to resolve an issue with a Onenote printer causing an unhandled null reference exception. 
                //This removes the row for null key values and continues the loop.
                if (key != null)
                {
                    string driver = key.GetValue("Printer Driver").ToString();

                    //Check to see if the HP UPD PCL 5 is in use for each queue
                    if (Regex.IsMatch(driver, "HP Universal Printing PCL 5 *"))
                    {
                        //If yes, use a "Check" image
                        printerDataGridView.Rows[currentRow].Cells[2].Value = Properties.Resources.Check;
                    }

                    else
                    {
                        //If no, use a "X" image
                        printerDataGridView.Rows[currentRow].Cells[2].Value = Properties.Resources.X;
                        //printerDataGridView.Rows[currentRow].ReadOnly = true;
                    }

                    //Increment the counter at the end of the iteration
                    currentRow++;
                }

                else
                {
                    printerDataGridView.Rows.RemoveAt(currentRow);
                }
            }
            //Sort by Printer Name
            printerDataGridView.Sort(printerDataGridView.Columns[1], ListSortDirection.Ascending);
        }

        private void printerDataGridView_Enter(object sender, EventArgs e)
        {
            printerSelectionToolTip.Hide(toolTipPlaceHolder);
        }

        private void selectAllTestPageCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            //Check all test pages for printers that are selected if the checkbox is enabled and deselect all if the checkbox is disabled
            if (selectAllTestPageCheckBox.Checked == true)
            {
                int currentRow = 0;
                foreach (DataGridViewRow row in printerDataGridView.Rows)
                {
                    if (Convert.ToBoolean(row.Cells[0].Value) == true)
                    {
                        row.Cells[3].Value = true;
                    }

                    else
                    {
                        row.Cells[3].Value = false;
                    }

                    currentRow++;
                }
            }

            else
            {
                foreach (DataGridViewRow row in printerDataGridView.Rows)
                {
                    row.Cells[3].Value = false;
                }
            }
        }

        private void printerDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //Check to ensure that the row CheckBox is clicked
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                //Reference the current row
                DataGridViewRow row = printerDataGridView.Rows[e.RowIndex];

                //Toggle readonly, value, and cell/row color
                if (Convert.ToBoolean(row.Cells[0].Value) == false)
                {
                    row.Cells[3].Value = false;
                    row.Cells[3].ReadOnly = true;
                    row.DefaultCellStyle.BackColor = SystemColors.ControlLight;
                    row.Cells[0].Style.BackColor = Color.White;
                }

                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    row.Cells[3].ReadOnly = false;
                    row.DefaultCellStyle.BackColor = Color.White;

                    //Check testPageColumn for the row if selectAllTestPageCheckBox is checked
                    if (selectAllTestPageCheckBox.Checked == true)
                    {
                        row.Cells[3].Value = true;
                    }
                }
            }
        }

        private void printerDataGridView_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            //EndEdit on MouseUp of the checkBoxColumn
            //This helps the items in printerDataGridView_CellValueChanged be more responsive
            if (e.RowIndex >= 0 && e.ColumnIndex == 0)
            {
                printerDataGridView.EndEdit();
            }
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            //Close the current form and move back to the previous one
            this.Close();
            installTypeForm installTypeForm = new installTypeForm();
            installTypeForm.Show();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {

            //Save the queue names of checked rows to the checkedRows and checkdTestPages lists
            foreach (DataGridViewRow row in printerDataGridView.Rows)
            {
                if (Convert.ToBoolean(row.Cells[0].Value) == true)
                {
                    Globals.checkedRows.Add(row.Cells[1].Value.ToString());
                    Globals.checkedTestPages.Add(Convert.ToBoolean(row.Cells[3].Value));
                }
            }

            //Verify a printer has been selected
            if (Globals.checkedRows.Count == 0)
            {
                printerSelectionToolTip.Hide(toolTipPlaceHolder);
                printerSelectionToolTip.Show(@"Please select a print queue.", toolTipPlaceHolder, 5000);
            }

            else
            {
                //Close the current form and move to the next one
                this.Hide();
                fontSelectionForm fontSelectionForm = new fontSelectionForm();
                fontSelectionForm.Show();
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            //Exit the application
            Application.Exit();
        }
    }
}
