﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstallationUtility
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            agreementForm agreementForm = new agreementForm();
            agreementForm.Show();
            //Form1 form1 = new Form1();
            //form1.Show();
            Application.Run();
        }
    }
}
