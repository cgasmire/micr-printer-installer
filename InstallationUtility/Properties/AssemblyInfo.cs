﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MICR Printer Installer")]
[assembly: AssemblyDescription("Utility designed to automate installation of printer drivers and fonts used with TROY MICR printers.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("TROY Group, Inc.")]
[assembly: AssemblyProduct("MICR Printer Installer")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fca8da90-3f36-47e5-b851-0e6098deb0e1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.3")]
[assembly: AssemblyFileVersion("1.1.0.3")]
[assembly: NeutralResourcesLanguage("en-US")]

