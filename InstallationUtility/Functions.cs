﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Management;
using System.Threading;

//Errors generated from this section will have an Event ID in the range of 100-199

namespace InstallationUtility
{
    public class Functions
    {
        /// <summary>
        /// Unpacks files that are going to be used during installation to the users temp directory 
        /// or the standard location the HP Universal Print driver unzips to by default.
        /// </summary>

        public static void unpackFiles()
        {
            string tempFolder = Path.GetTempPath() + @"TROY Installer Files\";
            string driverFolder = @"C:\HP Universal Print Driver\";
            string uffFolder = @"C:\Windows\System32\spool\drivers\unifont\";
            bool osTypeIs64Bit = System.Environment.Is64BitOperatingSystem;
            string x64Dir = driverFolder + @"pcl5-x64-6.1.0.20062\";
            string x32Dir = driverFolder + @"pcl5-x32-6.1.0.20062\";
            string x64ZipFile = driverFolder + @"pcl5-x64-6.1.0.20062.zip";
            string x32ZipFile = driverFolder + @"pcl5-x32-6.1.0.20062.zip";

            //If tempFolder already exists, delete it and start fresh
            if (Directory.Exists(tempFolder))
            {
                try
                {
                    File.SetAttributes(tempFolder, FileAttributes.Normal);
                    Directory.Delete(tempFolder, true);
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 100);
                }
            }

            //Create tempFolder directory
            try
            {
                Directory.CreateDirectory(tempFolder);
                File.SetAttributes(tempFolder, FileAttributes.Normal);
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 101);
                DialogResult message = MessageBox.Show("Could not access Temp directory to create working directory. Please select a new location as a working directory.", "Temp directory inaccessible", MessageBoxButtons.OK);

                if (message == DialogResult.OK)
                {
                    //Created code for a temporary thread here to resolve the following error when this condition occurs:
                    //System.Threading.ThreadStateException: Current thread must be set to single thread apartment (STA) mode before OLE calls can be made. 
                    //Ensure that your Main function has STAThreadAttribute marked on it. This exception is only raised if a debugger is attached to the process.
                    //
                    //Code has been commented out as it appears this condition can only occur during debugging.

                    //var t = new Thread((ThreadStart)(() =>
                    //{
                        FolderBrowserDialog browser = new FolderBrowserDialog();
                        browser.ShowDialog();
                        browser.SelectedPath = tempFolder;
                    //}));

                    //t.SetApartmentState(ApartmentState.STA);
                    //t.Start();
                    //t.Join();
                }

                try
                {
                    Directory.CreateDirectory(tempFolder);
                    File.SetAttributes(tempFolder, FileAttributes.Normal);
                }
                catch (Exception e2)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e2.Message + " Trace:" + e2.StackTrace, EventLogEntryType.Error, 101);
                    DialogResult message2 = MessageBox.Show("Operation failed again. Please check permissions on Temp directory or selected working directory and run the installer again.", "Directory inaccessible", MessageBoxButtons.OK);

                    if (message2 == DialogResult.OK)
                    {
                        Application.Exit();
                    }
                }
            }

            //Copy test files
            try
            {
                File.WriteAllBytes(tempFolder + @"MICRFontTest.rtf", Properties.Resources.MICRFontTest);
                File.WriteAllBytes(tempFolder + @"SignatureTest.rtf", Properties.Resources.SignatureTest);

                Globals.micrTestPath = tempFolder + @"MICRFontTest.rtf";
                Globals.signatureTestPath = tempFolder + @"SignatureTest.rtf";
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 173);
            }

            //Copy PCM Files and unzip
            try
            {
                File.WriteAllBytes(tempFolder + @"PCMFiles.zip", Properties.Resources.PCMFiles);
                ZipFile.ExtractToDirectory(tempFolder + @"PCMFiles.zip", tempFolder + @"\PCMFiles");
            }
            catch (Exception e)
            {
                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 102);
            }

            //If uffFolder directory doesn't exist, create it
            if (!Directory.Exists(uffFolder))
            {
                try
                {
                    Directory.CreateDirectory(uffFolder);
                    File.SetAttributes(uffFolder, FileAttributes.Normal);
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 103);
                }
            }

            //Unpack UFF file
            if (!File.Exists(uffFolder + "AllTroyFonts64.UFF") && osTypeIs64Bit)
            {
                try
                {
                    File.WriteAllBytes(uffFolder + @"AllTroyFonts64.UFF", Properties.Resources.AllTroyFonts64);
                    File.SetAttributes(uffFolder + @"AllTroyFonts64.UFF", FileAttributes.Normal);
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 104);
                }
            }

            else if (!File.Exists(uffFolder + "AllTroyFonts32.UFF") && !osTypeIs64Bit)
            {
                try
                {
                    File.WriteAllBytes(uffFolder + @"AllTroyFonts32.UFF", Properties.Resources.AllTroyFonts32);
                    File.SetAttributes(uffFolder + @"AllTroyFonts32.UFF", FileAttributes.Normal);
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 105);
                }
            }

            //Unpack driver files
            switch (Globals.installationType)
            {
                case "Full Install":

                    //TODO - Put in logic to handle when user selects checkbox to install both system types

                    if (osTypeIs64Bit == true)
                    {
                        //If the osType is x64
                        if (!File.Exists(x64Dir + @"install.exe"))
                        {
                            Console.WriteLine("File has been found.");
                                //Create directory
                                try
                                {
                                    Directory.CreateDirectory(driverFolder);
                                    File.SetAttributes(driverFolder, FileAttributes.Normal);
                                }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 106);
                                }

                                //Copy PCL5 x64 driver and unzip
                                try
                                {
                                    File.WriteAllBytes(x64ZipFile, Properties.Resources.pcl5_x64_6_1_0_20062);
                                    //ZipFile.ExtractToDirectory(driverFolder + @"pcl5-x64-6.1.0.20062.zip", driverFolder + @"\pcl5-x64-6.1.0.20062");

                                DirectoryInfo di = Directory.CreateDirectory(x64Dir);
                                string destinationDirectoryFullPath = di.FullName;

                                using (ZipArchive archive = ZipFile.OpenRead(x64ZipFile))
                                {
                                    foreach (ZipArchiveEntry file in archive.Entries)
                                    {
                                        string completeFileName = Path.GetFullPath(Path.Combine(destinationDirectoryFullPath, file.FullName));

                                        if (!completeFileName.StartsWith(destinationDirectoryFullPath, StringComparison.OrdinalIgnoreCase))
                                        {
                                            //See this link for more info: https://snyk.io/research/zip-slip-vulnerability
                                            throw new IOException("Trying to extract file outside of destination directory.");
                                        }

                                        if (file.Name == "")
                                        {// Assuming Empty for Directory
                                            Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
                                            continue;
                                        }
                                        file.ExtractToFile(completeFileName, true);
                                    }
                                }
                            }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 107);
                                }
                            }
                        }

                    else
                    {
                        //If the osType is x32
                        if (!File.Exists(x32Dir + @"\install.exe"))
                        {

                                //Create directory
                                try
                                {
                                    Directory.CreateDirectory(driverFolder);
                                    File.SetAttributes(driverFolder, FileAttributes.Normal);
                                }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 108);
                                }

                                //Copy PCL5 x32 driver and unzip
                                try
                                {
                                    File.WriteAllBytes(x32ZipFile, Properties.Resources.pcl5_x32_6_1_0_20062);
                                //ZipFile.ExtractToDirectory(driverFolder + @"pcl5-x32-6.1.0.20062.zip", driverFolder + @"\pcl5-x32-6.1.0.20062");

                                DirectoryInfo di = Directory.CreateDirectory(x32Dir);
                                string destinationDirectoryFullPath = di.FullName;

                                using (ZipArchive archive = ZipFile.OpenRead(x32ZipFile))
                                {
                                    foreach (ZipArchiveEntry file in archive.Entries)
                                    {
                                        string completeFileName = Path.GetFullPath(Path.Combine(destinationDirectoryFullPath, file.FullName));

                                        if (!completeFileName.StartsWith(destinationDirectoryFullPath, StringComparison.OrdinalIgnoreCase))
                                        {
                                            //See this link for more info: https://snyk.io/research/zip-slip-vulnerability
                                            throw new IOException("Trying to extract file outside of destination directory.");
                                        }

                                        if (file.Name == "")
                                        {// Assuming Empty for Directory
                                            Directory.CreateDirectory(Path.GetDirectoryName(completeFileName));
                                            continue;
                                        }
                                        file.ExtractToFile(completeFileName, true);
                                    }
                                }
                            }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 109);
                                }
                            }
                        }

                    break;

                case "Full Both System Type":

                    //Unpack x64 files
                    if (!Directory.Exists(driverFolder + "pcl5-x64-6.1.0.20062"))
                    {
                        if (!File.Exists(driverFolder + @"pcl5-x64-6.1.0.20062\install.exe"))
                        { 
                            
                            //Create directory
                            try
                            {
                                Directory.CreateDirectory(driverFolder);
                                File.SetAttributes(driverFolder, FileAttributes.Normal);
                            }
                            catch (Exception e)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 110);
                            }

                            //Copy PCL5 x64 driver and unzip
                            try
                            {
                                File.WriteAllBytes(driverFolder + @"pcl5-x64-6.1.0.20062.zip", Properties.Resources.pcl5_x64_6_1_0_20062);
                                ZipFile.ExtractToDirectory(driverFolder + @"pcl5-x64-6.1.0.20062.zip", driverFolder + @"\pcl5-x64-6.1.0.20062");
                            }
                            catch (Exception e)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 111);
                            }
                        }
                    }

                    //Unpack x32 files
                    if (!Directory.Exists(driverFolder + "pcl5-x32-6.1.0.20062"))
                    {
                        if (!File.Exists(driverFolder + @"pcl5-x32-6.1.0.20062\install.exe"))
                        {
                            //Create directory
                            try
                            {
                                Directory.CreateDirectory(driverFolder);
                                File.SetAttributes(driverFolder, FileAttributes.Normal);
                            }
                            catch (Exception e)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 112);
                            }

                            //Copy PCL5 x32 driver and unzip
                            try
                            {
                                File.WriteAllBytes(driverFolder + @"pcl5-x32-6.1.0.20062.zip", Properties.Resources.pcl5_x32_6_1_0_20062);
                                ZipFile.ExtractToDirectory(driverFolder + @"pcl5-x32-6.1.0.20062.zip", driverFolder + @"\pcl5-x32-6.1.0.20062");
                            }
                            catch (Exception e)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 113);
                            }
                        }
                    }

                    break;

                case "Existing Queue":

                    //No driver files required for this installation type

                    break;

                case "TTFs Only":

                    //No driver files required for this installation type

                    break;

                case "Full Multi":

                    //TODO

                    break;

            }
        }


        /// <summary>
        /// Installs the TCP\IP port if the user has selected to create one.
        /// </summary>

        public static void installPort()
        {
            //Creates a TCP/IP port object using the IP address specified by the user.
            ManagementClass portClass = new ManagementClass("Win32_TCPIPPrinterPort");
            ManagementObject portObject = portClass.CreateInstance();
            portObject["Name"] = Globals.printerPort;
            portObject["HostAddress"] = Globals.printerPort;
            portObject["PortNumber"] = "9100";
            portObject["Protocol"] = 1;
            portObject["SNMPCommunity"] = "public";
            portObject["SNMPEnabled"] = true;
            portObject["SNMPDevIndex"] = 1;
            PutOptions options = new PutOptions();
            options.Type = PutType.UpdateOrCreate;

            try
            {
                //Add the newly created object
                portObject.Put(options);
            }

            catch (Exception e)
            {
                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 170);
            }


            GetAvailablePorts portsList = new GetAvailablePorts();
            bool portCreated = false;

            //Check to see if the port got created. Loop through a few times to give the system time to finish up.
            for (int i = 0; i < 5; i++)
            {
                foreach (var port in portsList.GetPortNames())
                {
                    if (port == Globals.printerPort)
                    {
                        portCreated = true;
                        i = 5;
                        //Console.WriteLine("Port has been found.");
                        break;
                    }

                    else
                    {
                        portCreated = false;
                    }
                }

                if (i > 5)
                {
                    //Console.WriteLine("New port not found. Sleeping for 5 seconds...");
                    System.Threading.Thread.Sleep(5000);
                    //Console.WriteLine("Continuing loop...");
                }
            }

            if (portCreated)
            {
                EventLog.WriteEntry("Troy MICR Printer Installer", @"Standard TCP/IP port created successfully.", EventLogEntryType.Information, 171);
            }

            else
            {
                EventLog.WriteEntry("Troy MICR Printer Installer", @"Initial attempt to create standard TCP/IP port unsuccessful. HP UPD will create the port.", EventLogEntryType.Error, 172);
            }
        }


            /// <summary>
            /// Runs a silent installer executable for the HP Universal Printing PCL 5 (6.1) driver
            /// to install printers with user defined variables populated on the fullInstallation form.
            /// </summary>

            public static void installDriver()
        {
            bool osTypeIs64Bit = System.Environment.Is64BitOperatingSystem;

            if (osTypeIs64Bit == true)
            {
                ProcessStartInfo processInfo = new ProcessStartInfo();
                Process installProcess;

                processInfo.FileName = @"C:\HP Universal Print Driver\pcl5-x64-6.1.0.20062\install.exe";
                processInfo.Arguments = "/q /h /npf /sm" + Globals.printerPort + " /n\"" + Globals.printerName + "\" /nd /u";


                //installProcess = Process.Start(processInfo);

                try
                {
                    //Commenting out to test
                    //installProcess.WaitForExit();

                    using (installProcess = Process.Start(processInfo))
                    {
                        installProcess.WaitForExit();
                    }
                }

                

                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 114);
                }
            }

            else
            {
                ProcessStartInfo processInfo = new ProcessStartInfo();
                Process installProcess;

                processInfo.FileName = @"C:\HP Universal Print Driver\pcl5-x32-6.1.0.20062\install.exe";
                processInfo.Arguments = "/q /h /npf /sm" + Globals.printerPort + " /n\"" + Globals.printerName + "\" /nd /u";

                //installProcess = Process.Start(processInfo);

                try
                {
                    //Commenting out to test
                    //installProcess.WaitForExit();

                    using (installProcess = Process.Start(processInfo))
                    {
                        installProcess.WaitForExit();
                    }
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 115);
                }
            }
        }


        /// <summary>
        /// Installs the TrueType fonts corresponding to the user's selection in the fontSelection form
        /// to the standard Windows Fonts directory and updates registry values validating the new fonts.
        /// </summary>

        public static void installTTFs()
        {
            string tempFolder = Path.GetTempPath() + @"TROY Installer Files\PCMFiles\";
            string fontFolder = @"\\localhost\c$\Windows\Fonts\";
            string domesticFonts = tempFolder + @"Domestic\Domestic Security Screen Fonts\";
            string internationalFonts = tempFolder + @"International\International Security Screen Fonts\";
            string seventyTwoFonts = tempFolder + @"Imaging (72pt)\Imaging (72pt) Screen Fonts\";
            string twelveFonts = tempFolder + @"Imaging (12pt)\Imaging (12pt) Screen Fonts\";
            string barcodeFonts = tempFolder + @"Barcode\Barcode Screen Fonts\";

            //Determine which TTF checkboxes are checked and install the appropriate TTFs
            foreach (string check in Globals.checkedFonts)
            {

                //Perform logic based on which checkboxes were checked
                switch (check)
                {
                    case "Domestic":

                        //This section has been commented out because overwriting/deleting existing fonts during the install has been causing 
                        //some fonts to become unusable until a reboot has occurred. A separate solution will be provided to manually uninstall fonts.

                        //bool domesticFontsExist = false;

                        ////Deletes any previously installed Domestic TTFs in the Fonts directory to avoid errors when unzipping the fonts to the Fonts driectory
                        //var g = Task.Run(() =>
                        //{
                        //    try
                        //    {
                        //        foreach (string file in Directory.GetFiles(domesticFonts))
                        //        {
                        //            //If the selected font already exists, delete it
                        //            if (File.Exists(fontFolder + Path.GetFileName(file)))
                        //            {
                        //                try
                        //                {
                        //                    domesticFontsExist = true;
                        //                    File.SetAttributes(fontFolder + Path.GetFileName(file), FileAttributes.Normal);
                        //                    File.Delete(fontFolder + Path.GetFileName(file));
                        //                }

                        //                //If unable to delete the selected font using the preferred method, run a batch file that deletes the fonts
                        //                catch (UnauthorizedAccessException er1)
                        //                {
                        //                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Warning, 116);

                        //                    if (domesticFontsExist)
                        //                    {
                        //                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        //                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                        //                        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        //                        startInfo.FileName = tempFolder + "DeleteDomestic.bat";
                        //                        startInfo.Verb = "runas";
                        //                        process.StartInfo = startInfo;

                        //                        try
                        //                        {
                        //                            process.Start();
                        //                            process.WaitForExit();
                        //                        }
                        //                        catch (Exception er2)
                        //                        {
                        //                            EventLog.WriteEntry("Troy MICR Printer Installer", er2.Message + " Trace:" + er2.StackTrace, EventLogEntryType.Error, 117);
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //    catch (Exception er1)
                        //    {
                        //        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 145);
                        //    }
                        //});

                        ////Wait until this task has completed to start installing Domestic fonts
                        //g.Wait();

                        //Unzip the Domestic TTFs to the Windows Fonts directory
                        var a = Task.Run(() =>
                        {
                            try
                            {
                                //ZipFile.ExtractToDirectory(tempFolder + "Domestic.zip", fontFolder);
                                Process process = new Process();
                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                startInfo.FileName = Path.Combine(tempFolder, "DomesticToFontDir.bat");
                                startInfo.Verb = "runas";

                                using (process = Process.Start(startInfo))
                                {
                                    process.WaitForExit();
                                }

                                //Commenting out to test
                                //process.StartInfo = startInfo;
                                //process.Start();
                                //process.WaitForExit();
                            }
                            catch (Exception er1)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 118);
                            }
                        });

                        //Wait until the task has completed to start updating the Domestic registry settings
                        a.Wait();

                        //Update registry information for Domestic fonts
                        foreach (string file in Directory.GetFiles(domesticFonts))
                        {
                            if (File.Exists(Path.Combine(fontFolder, file)))
                            {
                                string fileName = Path.GetFileName(file);
                                string fontName = Path.GetFileNameWithoutExtension(file);

                                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts");

                                try
                                {
                                    key.SetValue(fontName, fileName);
                                    key.Close();
                                }
                                catch (Exception er1)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 119);
                                }
                            }
                        }

                        break;

                    case "International":

                        //This section has been commented out because overwriting/deleting existing fonts during the install has been causing 
                        //some fonts to become unusable until a reboot has occurred. A separate solution will be provided to manually uninstall fonts.

                        //bool internationalFontsExist = false;

                        ////Deletes any previously installed International TTFs in the Fonts directory to avoid errors when unzipping the fonts to the Fonts driectory
                        //var h = Task.Run(() =>
                        //{
                        //    try
                        //    {
                        //        foreach (string file in Directory.GetFiles(internationalFonts))
                        //        {
                        //            //If the selected font already exists, delete it
                        //            if (File.Exists(fontFolder + Path.GetFileName(file)))
                        //            {
                        //                try
                        //                {
                        //                    internationalFontsExist = true;
                        //                    File.SetAttributes(fontFolder + Path.GetFileName(file), FileAttributes.Normal);
                        //                    File.Delete(fontFolder + Path.GetFileName(file));
                        //                }

                        //                //If unable to delete the selected font using the preferred method, run a batch file that deletes the fonts
                        //                catch (UnauthorizedAccessException er1)
                        //                {
                        //                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Warning, 120);

                        //                    if (internationalFontsExist)
                        //                    {
                        //                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        //                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                        //                        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        //                        startInfo.FileName = tempFolder + "DeleteInternational.bat";
                        //                        startInfo.Verb = "runas";
                        //                        process.StartInfo = startInfo;

                        //                        try
                        //                        {
                        //                            process.Start();
                        //                            process.WaitForExit();
                        //                        }
                        //                        catch (Exception er2)
                        //                        {
                        //                            EventLog.WriteEntry("Troy MICR Printer Installer", er2.Message + " Trace:" + er2.StackTrace, EventLogEntryType.Error, 121);
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //    catch (Exception er1)
                        //    {
                        //        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 146);
                        //    }
                        //});

                        ////Wait until this task has completed to start installing International fonts
                        //h.Wait();

                        //Unzip the International TTFs to the Windows Fonts directory
                        var b = Task.Run(() =>
                        {
                            try
                            {
                                //ZipFile.ExtractToDirectory(tempFolder + "International.zip", fontFolder);
                                Process process = new Process();
                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                startInfo.FileName = Path.Combine(tempFolder, "InternationalToFontDir.bat");
                                startInfo.Verb = "runas";

                                using (process = Process.Start(startInfo))
                                {
                                    process.WaitForExit();
                                }

                                //Commenting out to test
                                //process.StartInfo = startInfo;
                                //process.Start();
                                //process.WaitForExit();
                            }
                            catch (Exception er1)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 122);
                            }
                        });

                        //Wait until the task has completed to start updating the International registry settings
                        b.Wait();

                        //Update registry information for International fonts
                        foreach (string file in Directory.GetFiles(internationalFonts))
                        {
                            if (File.Exists(Path.Combine(fontFolder, file)))
                            {
                                string fileName = Path.GetFileName(file);
                                string fontName = Path.GetFileNameWithoutExtension(file);

                                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts");

                                try
                                {
                                    key.SetValue(fontName, fileName);
                                    key.Close();
                                }
                                catch (Exception er1)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 123);
                                }
                            }
                        }

                        break;

                    case "72pt":

                        //This section has been commented out because overwriting/deleting existing fonts during the install has been causing 
                        //some fonts to become unusable until a reboot has occurred. A separate solution will be provided to manually uninstall fonts.

                        //bool seventyTwoFontsExist = false;

                        ////Deletes any previously installed 72pt TTFs in the Fonts directory to avoid errors when unzipping the fonts to the Fonts driectory
                        //var i = Task.Run(() =>
                        //{
                        //    try
                        //    {
                        //        foreach (string file in Directory.GetFiles(seventyTwoFonts))
                        //        {
                        //            //If the selected font already exists, delete it
                        //            if (File.Exists(fontFolder + Path.GetFileName(file)))
                        //            {
                        //                try
                        //                {
                        //                    seventyTwoFontsExist = true;

                        //                    try
                        //                    {
                        //                        File.SetAttributes(fontFolder + Path.GetFileName(file), FileAttributes.Normal);
                        //                        File.Delete(fontFolder + Path.GetFileName(file));
                        //                    }
                        //                    catch (Exception er1)
                        //                    {
                        //                        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 124);
                        //                    }
                        //                }

                        //                //If unable to delete the selected font using the preferred method, run a batch file that deletes the fonts
                        //                catch (UnauthorizedAccessException er2)
                        //                {
                        //                    EventLog.WriteEntry("Troy MICR Printer Installer", er2.Message + " Trace:" + er2.StackTrace, EventLogEntryType.Warning, 125);

                        //                    if (seventyTwoFontsExist)
                        //                    {
                        //                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        //                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                        //                        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        //                        startInfo.FileName = tempFolder + "Delete72pt.bat";
                        //                        startInfo.Verb = "runas";
                        //                        process.StartInfo = startInfo;

                        //                        try
                        //                        {
                        //                            process.Start();
                        //                            process.WaitForExit();
                        //                        }
                        //                        catch (Exception er3)
                        //                        {
                        //                            EventLog.WriteEntry("Troy MICR Printer Installer", er3.Message + " Trace:" + er3.StackTrace, EventLogEntryType.Error, 126);
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //    catch (Exception er1)
                        //    {
                        //        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 147);
                        //    }
                        //});

                        ////Wait until this task has completed to start installing 72pt fonts
                        //i.Wait();

                        //Unzip the 72pt TTFs to the Windows Fonts directory
                        var c = Task.Run(() =>
                        {
                            try
                            {
                                //ZipFile.ExtractToDirectory(tempFolder + "72pt.zip", fontFolder);
                                Process process = new Process();
                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                startInfo.FileName = Path.Combine(tempFolder, "72ToFontDir.bat");
                                startInfo.Verb = "runas";
                                using (process = Process.Start(startInfo))
                                {
                                    process.WaitForExit();
                                }

                                //Commenting out to test
                                //process.StartInfo = startInfo;
                                //process.Start();
                                //process.WaitForExit();
                            }
                            catch (Exception er1)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 127);
                            }
                        });

                        //Wait until the task has completed to start updating the 72pt registry settings
                        c.Wait();

                        //Update registry information for 72pt fonts
                        foreach (string file in Directory.GetFiles(seventyTwoFonts))
                        {
                            if (File.Exists(Path.Combine(fontFolder, file)))
                            {
                                string fileName = Path.GetFileName(file);
                                string fontName = Path.GetFileNameWithoutExtension(file);

                                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts");

                                try
                                {
                                    key.SetValue(fontName, fileName);
                                    key.Close();
                                }
                                catch (Exception er1)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 128);
                                }
                            }
                        }

                        break;

                    case "12pt":

                        //This section has been commented out because overwriting/deleting existing fonts during the install has been causing 
                        //some fonts to become unusable until a reboot has occurred. A separate solution will be provided to manually uninstall fonts.

                        //bool twelveFontsExist = false;

                        ////Deletes any previously installed 12pt TTFs in the Fonts directory to avoid errors when unzipping the fonts to the Fonts driectory
                        //var j = Task.Run(() =>
                        //{
                        //    try
                        //    {
                        //        foreach (string file in Directory.GetFiles(twelveFonts))
                        //        {

                        //            //If the selected font already exists, delete it
                        //            if (File.Exists(fontFolder + Path.GetFileName(file)))
                        //            {
                        //                try
                        //                {
                        //                    twelveFontsExist = true;

                        //                    try
                        //                    {
                        //                        File.SetAttributes(fontFolder + Path.GetFileName(file), FileAttributes.Normal);
                        //                        File.Delete(fontFolder + Path.GetFileName(file));
                        //                    }
                        //                    catch (Exception er1)
                        //                    {
                        //                        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 129);
                        //                    }
                        //                }

                        //                //If unable to delete the selected font using the preferred method, run a batch file that deletes the fonts
                        //                catch (UnauthorizedAccessException er1)
                        //                {
                        //                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Warning, 130);

                        //                    if (twelveFontsExist)
                        //                    {
                        //                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        //                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                        //                        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        //                        startInfo.FileName = tempFolder + "Delete12pt.bat";
                        //                        startInfo.Verb = "runas";
                        //                        process.StartInfo = startInfo;

                        //                        try
                        //                        {
                        //                            process.Start();
                        //                            process.WaitForExit();
                        //                        }
                        //                        catch (Exception er2)
                        //                        {
                        //                            EventLog.WriteEntry("Troy MICR Printer Installer", er2.Message + " Trace:" + er2.StackTrace, EventLogEntryType.Error, 131);
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //    catch (Exception er1)
                        //    {
                        //        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 148);
                        //    }
                        //});

                        ////Wait until this task has completed to start installing 12pt fonts
                        //j.Wait();

                        //Unzip the 12pt TTFs to the Windows Fonts directory
                        var e = Task.Run(() =>
                        {
                            try
                            {
                                //ZipFile.ExtractToDirectory(tempFolder + "12pt.zip", fontFolder);
                                Process process = new Process();
                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                startInfo.FileName = Path.Combine(tempFolder, "12ToFontDir.bat");
                                startInfo.Verb = "runas";

                                using (process = Process.Start(startInfo))
                                {
                                    process.WaitForExit();
                                }

                                //Commenting out to test
                                //process.StartInfo = startInfo;
                                //process.Start();
                                //process.WaitForExit();
                            }
                            catch (Exception er1)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 132);
                            }
                        });

                        //Wait until the task has completed to start updating the 12pt registry settings
                        e.Wait();

                        //Update registry information for 12pt fonts
                        foreach (string file in Directory.GetFiles(twelveFonts))
                        {
                            if (File.Exists(Path.Combine(fontFolder, file)))
                            {
                                string fileName = Path.GetFileName(file);
                                string fontName = Path.GetFileNameWithoutExtension(file);

                                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts");

                                try
                                {
                                    key.SetValue(fontName, fileName);
                                    key.Close();
                                }
                                catch (Exception er1)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 133);
                                }
                            }
                        }

                        break;

                    case "Barcode":

                        //This section has been commented out because overwriting/deleting existing fonts during the install has been causing 
                        //some fonts to become unusable until a reboot has occurred. A separate solution will be provided to manually uninstall fonts.

                        //bool barcodeFontsExist = false;

                        ////Deletes any previously installed Barcode TTFs in the Fonts directory to avoid errors when unzipping the fonts to the Fonts driectory
                        //var k = Task.Run(() =>
                        //{
                        //    try
                        //    {
                        //        foreach (string file in Directory.GetFiles(barcodeFonts))
                        //        {

                        //            //If the selected font already exists, delete it
                        //            if (File.Exists(fontFolder + Path.GetFileName(file)))
                        //            {
                        //                try
                        //                {
                        //                    barcodeFontsExist = true;

                        //                    try
                        //                    {
                        //                        File.SetAttributes(fontFolder + Path.GetFileName(file), FileAttributes.Normal);
                        //                        File.Delete(fontFolder + Path.GetFileName(file));
                        //                    }
                        //                    catch (Exception er1)
                        //                    {
                        //                        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 134);
                        //                    }
                        //                }

                        //                //If unable to delete the selected font using the preferred method, run a batch file that deletes the fonts
                        //                catch (UnauthorizedAccessException er1)
                        //                {
                        //                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Warning, 135);

                        //                    if (barcodeFontsExist)
                        //                    {
                        //                        System.Diagnostics.Process process = new System.Diagnostics.Process();
                        //                        System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                        //                        startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                        //                        startInfo.FileName = tempFolder + "DeleteBarcode.bat";
                        //                        startInfo.Verb = "runas";
                        //                        process.StartInfo = startInfo;

                        //                        try
                        //                        {
                        //                            process.Start();
                        //                            process.WaitForExit();
                        //                        }
                        //                        catch (Exception er2)
                        //                        {
                        //                            EventLog.WriteEntry("Troy MICR Printer Installer", er2.Message + " Trace:" + er2.StackTrace, EventLogEntryType.Error, 136);
                        //                        }
                        //                    }
                        //                }
                        //            }
                        //        }
                        //    }
                        //    catch (Exception er1)
                        //    {
                        //        EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 149);
                        //    }
                        //});

                        ////Wait until this task has completed to start installing Barcode fonts
                        //k.Wait();

                        //Unzip the Barcode TTFs to the Windows Fonts directory
                        var f = Task.Run(() =>
                        {
                            try
                            {
                                //ZipFile.ExtractToDirectory(tempFolder + "Barcode.zip", fontFolder);
                                Process process = new Process();
                                ProcessStartInfo startInfo = new ProcessStartInfo();
                                startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                                startInfo.FileName = Path.Combine(tempFolder, "BarcodeToFontDir.bat");
                                startInfo.Verb = "runas";

                                using (process = Process.Start(startInfo))
                                {
                                    process.WaitForExit();
                                }

                                //Commenting out to test
                                //process.StartInfo = startInfo;
                                //process.Start();
                                //process.WaitForExit();
                            }
                            catch (Exception er1)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 137);
                            }
                        });

                        //Wait until the task has completed to start updating the Barcode registry settings
                        f.Wait();

                        //Update registry information for Barcode fonts
                        foreach (string file in Directory.GetFiles(barcodeFonts))
                        {
                            if (File.Exists(Path.Combine(fontFolder, file)))
                            {
                                string fileName = Path.GetFileName(file);
                                string fontName = Path.GetFileNameWithoutExtension(file);

                                Microsoft.Win32.RegistryKey key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts");

                                try
                                {
                                    key.SetValue(fontName, fileName);
                                    key.Close();
                                }
                                catch (Exception er1)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", er1.Message + " Trace:" + er1.StackTrace, EventLogEntryType.Error, 138);
                                }
                            }
                        }

                        break;
                }
            }
        }


        /// <summary>
        /// Sets registry keys for printers created in the fullInstallationForm or selected in the queueSelectionForm.
        /// Allows the print queue to see the UFF file used for external fonts and registers the appropriate substitutions
        /// on the Font Substitution Table.
        /// </summary>

        public static void setRegistryKeys()
        {
            string externalFontFile32Key = @"C:\Windows\System32\spool\drivers\unifont\AllTroyFonts32.UFF";
            string externalFontFile64Key = @"C:\Windows\System32\spool\drivers\unifont\AllTroyFonts64.UFF";
            string[] ttfFontSubTableKey = { "Courier New", "Courier New", "Troy Auto-Protect Screen Font", "TROY Auto-Protect", "Troy Barcode 128 Screen Font", "TROY BC128", "Troy Barcode 2of5 Screen Font", "TROY BC2of5", "Troy Barcode 3of9 Screen Font", "TROY BC3of9", "Troy CMC-7 Screen Font", "TROY CMC7", "Troy E-13B Screen Font", "Troy E-13B", "Troy EAN-13 Screen Font", "TROY EAN-13", "Troy EAN-8 Screen Font", "TROY EAN-8", "Troy ECF Screen Font", "TROY ECF", "Troy Image 102 Screen Font", "Troy Image # 102 12pt", "Troy Image 103 Screen Font", "Troy Image # 103 12pt", "Troy Image 104 Screen Font", "Troy Image # 104 12pt", "Troy Image 105 Screen Font", "Troy Image # 105 12pt", "Troy Image 106 Screen Font", "Troy Image # 106 12pt", "Troy Image 107 Screen Font", "Troy Image # 107 12pt", "Troy Image 108 Screen Font", "Troy Image # 108 12pt", "Troy Image 109 Screen Font", "Troy Image # 109 12pt", "Troy Image 110 Screen Font", "Troy Image # 110 12pt", "Troy Image 111 Screen Font", "Troy Image # 111 12pt", "Troy Image 201 Screen Font", "TROY Image # 201 72pt", "Troy Image 202 Screen Font", "TROY Image # 202 72pt", "Troy Image 203 Screen Font", "TROY Image # 203 72pt", "Troy Image 204 Screen Font", "TROY Image # 204 72pt", "Troy Image 205 Screen Font", "TROY Image # 205 72pt", "Troy Image 206 Screen Font", "TROY Image # 206 72pt", "Troy Image 207 Screen Font", "TROY Image # 207 72pt", "Troy Image 208 Screen Font", "TROY Image # 208 72pt", "Troy Image 209 Screen Font", "TROY Image # 209 72pt", "Troy Image 210 Screen Font", "TROY Image # 210 72pt", "Troy LCF British Screen Font", "TROY LCF British", "Troy LCF French Screen Font", "TROY LCF French", "Troy LCF Italian Screen Font", "TROY LCF Italian", "Troy LCF Screen Font", "TROY LCF", "Troy LCF Spanish Screen Font", "TROY LCF Spanish", "Troy Micro Screen Font", "TROY Micro 1pt", "Troy OCR-A Screen Font", "TROY OCR-A", "Troy OCR-B Screen Font", "TROY OCR-B", "Troy Postnet Rev Screen Font", "TROY POSTNET Rev", "Troy Postnet Screen Font", "TROY POSTNET", "Troy Rev Helv Screen Font", "TROY HELV UpsideDown", "Troy SCF British Screen Font", "TROY SCF British", "Troy SCF French Screen Font", "TROY SCF French", "Troy SCF Italian Screen Font", "TROY SCF Italian", "Troy SCF Screen Font", "TROY SCF", "Troy Security Screen Font", "Troy Security", "Troy Security Spanish Screen Fo", "TROY Security Spanish", "Troy UPC-A Screen Font", "TROY UPC-A", "Troy UPC-E Screen Font", "TROY UPC-E" };
            int ttfFontSubTableSizeKey = 4242;
            bool osTypeIs64Bit = System.Environment.Is64BitOperatingSystem;

            switch (Globals.installationType)
            {
                case "Full Install":

                    //Set registry path to selectedPrinter
                    RegistryKey selectedPrinterKey1 = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Print\Printers\" + Globals.printerName, true);
                    RegistryKey printerDriverDataKey1 = selectedPrinterKey1.OpenSubKey("PrinterDriverData", true);

                    Console.WriteLine("printerDriverDataKey1:  " + printerDriverDataKey1);

                    //Set External Font and Font Substitution Table registry keys
                    if (selectedPrinterKey1 != null)
                    {
                        if (osTypeIs64Bit)
                        {
                            if (printerDriverDataKey1 != null)
                            {
                                try
                                {
                                    printerDriverDataKey1.SetValue("ExternalFontFile", externalFontFile64Key, RegistryValueKind.String);
                                }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 139);
                                }
                            }

                            else
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", "PrinterDriverData key not found. Installer will manually create this key.", EventLogEntryType.Information, 175);

                                selectedPrinterKey1.CreateSubKey("PrinterDriverData");

                                try
                                {
                                    printerDriverDataKey1.SetValue("ExternalFontFile", externalFontFile64Key, RegistryValueKind.String);
                                }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 176);
                                }
                            }
                        }

                        else
                        {
                            if (printerDriverDataKey1 != null)
                            {
                                try
                                {
                                    printerDriverDataKey1.SetValue("ExternalFontFile", externalFontFile32Key, RegistryValueKind.String);
                                }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 140);
                                }
                            }

                            else
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", "PrinterDriverData key not found. Installer will manually create this key.", EventLogEntryType.Information, 177);

                                selectedPrinterKey1.CreateSubKey("PrinterDriverData");

                                try
                                {
                                    printerDriverDataKey1.SetValue("ExternalFontFile", externalFontFile32Key, RegistryValueKind.String);
                                }
                                catch (Exception e)
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 178);
                                }
                            }
                        }

                        try
                        {
                            printerDriverDataKey1.SetValue("TTFontSubTable", ttfFontSubTableKey, RegistryValueKind.MultiString);
                            printerDriverDataKey1.SetValue("TTFontSubTableSize", ttfFontSubTableSizeKey, RegistryValueKind.DWord);
                            selectedPrinterKey1.Close();
                        }
                        catch (Exception e)
                        {
                            EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 141);
                        }
                    }

                    break;

                case "Existing Queue":

                    foreach (string selectedPrinter in Globals.checkedRows)
                    {
                        //Set registry path to selectedPrinter
                        RegistryKey selectedPrinterKey2 = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Print\Printers\" + selectedPrinter, true);
                        RegistryKey printerDriverDataKey2 = selectedPrinterKey2.OpenSubKey("PrinterDriverData", true);

                        Console.WriteLine("printerDriverDataKey2:  " + printerDriverDataKey2);

                        //Set External Font and Font Substitution Table registry keys
                        if (selectedPrinterKey2 != null)
                        {
                            if (osTypeIs64Bit)
                            {
                                if (printerDriverDataKey2 != null)
                                {
                                    try
                                    {
                                        printerDriverDataKey2.SetValue("ExternalFontFile", externalFontFile64Key, RegistryValueKind.String);
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 142);
                                    }
                                }

                                else
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", "PrinterDriverData key not found. Installer will manually create this key.", EventLogEntryType.Information, 179);

                                    selectedPrinterKey2.CreateSubKey("PrinterDriverData");

                                    try
                                    {
                                        printerDriverDataKey2.SetValue("ExternalFontFile", externalFontFile64Key, RegistryValueKind.String);
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 180);
                                    }
                                }
                            }

                            else
                            {
                                if (printerDriverDataKey2 != null)
                                {
                                    try
                                    {
                                        printerDriverDataKey2.SetValue("ExternalFontFile", externalFontFile32Key, RegistryValueKind.String);
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 143);
                                    }
                                }

                                else
                                {
                                    EventLog.WriteEntry("Troy MICR Printer Installer", "PrinterDriverData key not found. Installer will manually create this key.", EventLogEntryType.Information, 181);

                                    selectedPrinterKey2.CreateSubKey("PrinterDriverData");

                                    try
                                    {
                                        printerDriverDataKey2.SetValue("ExternalFontFile", externalFontFile32Key, RegistryValueKind.String);
                                    }
                                    catch (Exception e)
                                    {
                                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 182);
                                    }
                                }
                            }

                            try
                            {
                                printerDriverDataKey2.SetValue("TTFontSubTable", ttfFontSubTableKey, RegistryValueKind.MultiString);
                                printerDriverDataKey2.SetValue("TTFontSubTableSize", ttfFontSubTableSizeKey, RegistryValueKind.DWord);
                                selectedPrinterKey2.Close();
                            }
                            catch (Exception e)
                            {
                                EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 144);
                            }
                        }
                    }

                    break;

                case "TTFs Only":

                    //Do nothing, no print queue registry keys will be updated

                    break;

                case "Full Multi":

                    //TODO

                    break;
            }
        }


        public static void cancelInstallCheck()
        {
            RegistryKey selectedPrinterKey = Registry.LocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Control\Print\Printers\" + Globals.printerName, true);


            if (Globals.cancelInstallFlag)
            {
                var a = Task.Run(() =>
                {
                    DialogResult result = MessageBox.Show("Are you sure you want to stop the installation in progress?", "Cancel Installation?", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

                    if (result == DialogResult.Yes)
                    {
                        switch (Globals.installStatus)
                        {
                            case 0:
                                Console.WriteLine("0");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 0", EventLogEntryType.Information, 156);

                                Application.Exit();

                                break;

                            case 1:
                                Console.WriteLine("1");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 1", EventLogEntryType.Information, 157);

                                Application.Exit();

                                break;

                            case 2:
                                Console.WriteLine("2");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 2", EventLogEntryType.Information, 158);

                                Application.Exit();

                                //string machineName = System.Environment.MachineName;

                                //foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                                //{
                                //    if (printer == Globals.printerName)
                                //    {
                                //        ProcessStartInfo processInfo = new ProcessStartInfo();
                                //        Process deletePrinterProcess;

                                //        processInfo.FileName = @"cmd.exe";
                                //        processInfo.Arguments = "rundll32 printui.dll,PrintUIEntry /dl /n \"" + Globals.printerName + "\" /c\\" + machineName + "";

                                //        deletePrinterProcess = Process.Start(processInfo);
                                //        deletePrinterProcess.WaitForExit();

                                //        Console.WriteLine(processInfo.Arguments);

                                //        Application.Exit();
                                //    }
                                //}
                                break;

                            case 3:
                                Console.WriteLine("3");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 3", EventLogEntryType.Information, 159);

                                Application.Exit();

                                break;

                            case 4:
                                Console.WriteLine("4");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 4", EventLogEntryType.Information, 160);

                                Application.Exit();

                                break;

                            case 5:
                                Console.WriteLine("5");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 5", EventLogEntryType.Information, 161);

                                Application.Exit();

                                break;

                            case 6:
                                Console.WriteLine("6");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 6", EventLogEntryType.Information, 162);

                                Application.Exit();

                                break;

                            case 7:
                                Console.WriteLine("7");

                                EventLog.WriteEntry("Troy MICR Printer Installer", "Application exited during installation. Case 7", EventLogEntryType.Information, 163);

                                Application.Exit();

                                break;
                        }
                    }

                    else
                    {
                        Globals.cancelInstallFlag = false;
                    }
                });

                //Wait until the task has completed to start updating the Barcode registry settings
                a.Wait();
            }
        }


        /// <summary>
        /// Runs a batch file that rebuilds the Windows font cache located at C:\Windows\ServiceProfiles\LocalService\AppData\Local
        /// and C:\Windows\ServiceProfiles\LocalService\AppData\Local\FontCache. Stops the "Windows Font Cache Service" and "Windows Presentation 
        /// Foundation Font Cache 3.0.0.0" services, deletes all files in the cache directories with a *.dat extension, and then restarts the services.
        /// This process helps ensure the fonts can be used seamlessly without the need for a reboot after install.
        /// 
        /// ***IMPORTANT NOTE***
        /// Batch file has been pulled from the project until it is proven this method will resolve the issue it intends to resolve. The implementation
        /// in the loading form has been commented out. This function may be removed altogether in a later release.
        /// </summary>
        public static void rebuildFontCache()
        {
            string tempFolder = Path.GetTempPath() + @"TROY Installer Files\PCMFiles\";

            //Rebuild the Font cache
            var x = Task.Run(() =>
            {
                try
                {
                    Process process = new Process();
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    startInfo.FileName = Path.Combine(tempFolder, "RebuildFontCache.bat");
                    startInfo.Verb = "runas";

                    using (process = Process.Start(startInfo))
                    {
                        process.WaitForExit();
                    }

                    //Commenting out to test
                    //process.StartInfo = startInfo;
                    //process.Start();
                    //process.WaitForExit();
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 150);
                }
            });

            //Stop font cache services
            x.Wait();
        }

        /// <summary>
        /// Runs batch files that delete the TTF's from the Windows Fonts directory.
        /// Also deletes registry subkeys associated with the TROY fonts under the Fonts key.
        /// </summary>
        public static void initializeGlobals()
        {
            Globals.checkedRows = new List<string>();
            Globals.checkedFonts = new List<string>();
            Globals.installationType = "Full Installation";
            Globals.systemType = "One";
            Globals.printerName = "TROY MICR Printer";
            Globals.printerPort = null;
            Globals.loadingQueue = new Queue<Action>();
            Globals.statusLabel = "Loading components...";
            Globals.ipIsValid = false;
        }


        /// <summary>
        /// Runs batch files that delete the TTF's from the Windows Fonts directory.
        /// Also deletes registry subkeys associated with the TROY fonts under the Fonts key.
        /// </summary>
        public static void deleteFonts()
        {
            string tempFolder = Path.GetTempPath() + @"TROY Installer Files\PCMFiles\";
            string domesticFonts = tempFolder + @"Domestic\Domestic Security Screen Fonts\";
            string internationalFonts = tempFolder + @"International\International Security Screen Fonts\";
            string seventyTwoFonts = tempFolder + @"Imaging (72pt)\Imaging (72pt) Screen Fonts\";
            string twelveFonts = tempFolder + @"Imaging (12pt)\Imaging (12pt) Screen Fonts\";
            string barcodeFonts = tempFolder + @"Barcode\Barcode Screen Fonts\";

            if (Globals.checkedFonts.Contains("Domestic"))
            {
                //Run a batch file to delete the Domestic TTF's from the Windows Fonts directory
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = tempFolder + "DeleteDomestic.bat";
                startInfo.Verb = "runas";
                //process.StartInfo = startInfo;

                try
                {
                    using (process = Process.Start(startInfo))
                    {
                        process.WaitForExit();
                    }

                    //Commenting out to test
                    //process.Start();
                    //process.WaitForExit();
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 164);
                }

                //Delete the registry subkeys associated with the Domestic fonts from the Fonts key
                foreach (string file in Directory.GetFiles(domesticFonts))
                {
                    RegistryKey selectedFontKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts\", true);
                    string fileName = Path.GetFileNameWithoutExtension(file);

                    try
                    {
                        if (selectedFontKey != null)
                        {
                            selectedFontKey.DeleteValue(fileName);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 151);
                    }
                }
            }

            if (Globals.checkedFonts.Contains("International"))
            {
                //Run a batch file to delete the International TTF's from the Windows Fonts directory
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = tempFolder + "DeleteInternational.bat";
                startInfo.Verb = "runas";
                //process.StartInfo = startInfo;

                try
                {
                    using (process = Process.Start(startInfo))
                    {
                        process.WaitForExit();
                    }

                    //Commenting out to test
                    //process.Start();
                    //process.WaitForExit();
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 165);
                }

                //Delete the registry subkeys associated with the International fonts from the Fonts key
                foreach (string file in Directory.GetFiles(internationalFonts))
                {
                    RegistryKey selectedFontKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts\", true);
                    string fileName = Path.GetFileNameWithoutExtension(file);

                    try
                    {
                        //Prevent errors from trying to delete registry keys for fonts that exist in both Domestic and International font collections
                        if (selectedFontKey != null && !Globals.checkedFonts.Contains("Domestic"))
                        {
                            selectedFontKey.DeleteValue(fileName);
                        }

                        else if (selectedFontKey != null && Globals.checkedFonts.Contains("Domestic"))
                        {
                            switch (fileName)
                            {
                                //Do nothing for fonts that are duplicated in both Domestic and International
                                case "Troy Auto-Protect Screen Font":   break;
                                case "Troy CMC-7 Screen Font":          break;
                                case "Troy E-13B Screen Font":          break;
                                case "Troy ECF Screen Font":            break;
                                case "Troy LCF Screen Font":            break;
                                case "Troy Micro Screen Font":          break;
                                case "Troy OCR-A Screen Font":          break;
                                case "Troy OCR-B Screen Font":          break;
                                case "Troy Rev Helv Screen Font":       break;
                                case "Troy SCF Screen Font":            break;
                                case "Troy Security Screen Font":       break;

                                default:

                                    selectedFontKey.DeleteValue(fileName);

                                    break;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 152);
                    }
                }
            }

            if (Globals.checkedFonts.Contains("72pt"))
            {
                //Run a batch file to delete the 72pt TTF's from the Windows Fonts directory
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = tempFolder + "Delete72pt.bat";
                startInfo.Verb = "runas";
                //process.StartInfo = startInfo;

                try
                {
                    using (process = Process.Start(startInfo))
                    {
                        process.WaitForExit();
                    }

                    //Commenting out to test
                    //process.Start();
                    //process.WaitForExit();
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 166);
                }

                //Delete the registry subkeys associated with the 72pt fonts from the Fonts key
                foreach (string file in Directory.GetFiles(seventyTwoFonts))
                {
                    RegistryKey selectedFontKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts\", true);
                    string fileName = Path.GetFileNameWithoutExtension(file);

                    try
                    {
                        if (selectedFontKey != null)
                        {
                            selectedFontKey.DeleteValue(fileName);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 153);
                    }
                }
            }

            if (Globals.checkedFonts.Contains("12pt"))
            {
                //Run a batch file to delete the 12pt TTF's from the Windows Fonts directory
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = tempFolder + "Delete12pt.bat";
                startInfo.Verb = "runas";
                //process.StartInfo = startInfo;

                try
                {
                    using (process = Process.Start(startInfo))
                    {
                        process.WaitForExit();
                    }

                    //Commenting out to test
                    //process.Start();
                    //process.WaitForExit();
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 167);
                }

                //Delete the registry subkeys associated with the 12pt fonts from the Fonts key
                foreach (string file in Directory.GetFiles(twelveFonts))
                {
                    RegistryKey selectedFontKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts\", true);
                    string fileName = Path.GetFileNameWithoutExtension(file);

                    try
                    {
                        if (selectedFontKey != null)
                        {
                            selectedFontKey.DeleteValue(fileName);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 154);
                    }
                }
            }

            if (Globals.checkedFonts.Contains("Barcode"))
            {
                //Run a batch file to delete the Barcode TTF's from the Windows Fonts directory
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = tempFolder + "DeleteBarcode.bat";
                startInfo.Verb = "runas";
                //process.StartInfo = startInfo;

                try
                {
                    using (process = Process.Start(startInfo))
                    {
                        process.WaitForExit();
                    }

                    //Commenting out to test
                    //process.Start();
                    //process.WaitForExit();
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 168);
                }

                //Delete the registry subkeys associated with the Barcode fonts from the Fonts key
                foreach (string file in Directory.GetFiles(barcodeFonts))
                {
                    RegistryKey selectedFontKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts\", true);
                    string fileName = Path.GetFileNameWithoutExtension(file);

                    try
                    {
                        if (selectedFontKey != null)
                        {
                            selectedFontKey.DeleteValue(fileName);
                        }
                    }
                    catch (Exception e)
                    {
                        EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 155);
                    }
                }
            }            
        }

        public static void printDocument(string documentLocation, string printerName)
        {
            var a = Task.Run(() =>
            {
                Process printProcess = new Process();
                ProcessStartInfo info = new ProcessStartInfo();
                info.FileName = @"cmd.exe";
                info.Arguments = "/C write.exe /pt \"" + documentLocation + "\" " + printerName;
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;

                printProcess.StartInfo = info;
                //printProcess.Start();

                try
                {
                    using (printProcess = Process.Start(info))
                    {
                        printProcess.WaitForExit();
                    }
                }
                catch (Exception e)
                {
                    EventLog.WriteEntry("Troy MICR Printer Installer", e.Message + " Trace:" + e.StackTrace, EventLogEntryType.Error, 174);
                }
            });

            a.Wait();
        }
    }
}
