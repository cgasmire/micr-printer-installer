﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;

//Errors generated from this section will have an Event ID in the range of 400-499

namespace InstallationUtility
{
    public partial class fullInstallationForm : Form
    {
        public fullInstallationForm()
        {
            InitializeComponent();

            //Event handlers
            printerNameTextBox.KeyPress += printerNameTextBox_KeyPress;
            existingPortRadioButton.CheckedChanged += existingPortRadioButton_CheckedChanged;
            existingPortComboBox.SelectedIndexChanged += existingPortComboBox_SelectedIndexChanged;
            ipTextBox.Leave += ipTextBox_Leave;
            ipTextBox.KeyPress += ipTextBox_KeyPress;
            ipTextBox.TextChanged += ipTextBox_TextChanged;
        }

        private void fullInstallationForm_Load(object sender, EventArgs e)
        {
            versionLabel.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            //Load existing ports
            GetAvailablePorts portsList = new GetAvailablePorts();

            //Populate the existing port dropdown
            foreach (var port in portsList.GetPortNames())
            {
                existingPortComboBox.Items.Add(port);
            }

            //Initialize tooltips
            tcpipToolTip.SetToolTip(ipTextBox, "Required Format");
            printerNameToolTip.SetToolTip(printerNameTextBox, "Required Format");
            existingPortToolTip.SetToolTip(existingPortComboBox, "Select Port");

            existingPortComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void existingPortRadioButton_CheckedChanged(Object sender, EventArgs e)
        {
            //Enable or disable port selections depending on radio button position
            if (existingPortRadioButton.Checked == true)
            {
                existingPortComboBox.Enabled = true;
                ipTextBox.Enabled = false;
                ipErrorPictureBox.Visible = false;
                tcpipToolTip.Hide(ipTextBox);
            }

            else
            {
                existingPortComboBox.Enabled = false;
                ipTextBox.Enabled = true;
                existingPortErrorPictureBox.Visible = false;
                existingPortToolTip.Hide(existingPortComboBox);
            }
        }

        private void existingPortComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Hide tooltip/error when a selection has been made
            existingPortToolTip.Hide(existingPortComboBox);
            existingPortErrorPictureBox.Visible = false;
        }

        private void printerNameTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Warn the user if a non-acceptable character is pressed - the '/' and ',' characters cannot be used as a printer name in Windows
            if (e.KeyChar == '\\' || e.KeyChar == ',')
            {
                e.Handled = true;
                printerNameToolTip.Hide(printerNameTextBox);
                printerNameToolTip.Show(@"A printer name connot contain the characters '\' or ','.", printerNameTextBox, 5000);
            }
        }

        private void ipTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Mask the IP textbox so that only numbers and periods can be used
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != '.')
            {
                e.Handled = true;
            }
        }

        private void ipTextBox_Leave(object sender, EventArgs e)
        {
            Globals.printerPort = ipTextBox.Text;

            //Use RegEx to verify whether text in ipTextBox is in valid IP address format
            var Value = ipTextBox.Text;
            var Pattern = new string[]
            {
            "^",                                            // Start of string
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.",    // Between 000 and 255 and "."
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.",
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\.",
            @"([01]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])",      // Same as before, no period
            "$",                                            // End of string
            };

            var Match = Regex.IsMatch(Value, string.Join(string.Empty, Pattern));

            //
            int octet0 = 0;
            int octet1 = 0;
            int octet2 = 0;
            int octet3 = 0;

            //If ipTextBox is empty when focus is changed, remove tooltip/error
            if (String.IsNullOrEmpty(Value))
            {
                tcpipToolTip.Hide(ipTextBox);
                ipErrorPictureBox.Visible = false;
            }

            else
            {
                try
                {
                    //Separate each octet into individual variables
                    string[] octets = Value.Split('.');
                    octet0 = Int32.Parse(octets[0]);
                    octet1 = Int32.Parse(octets[1]);
                    octet2 = Int32.Parse(octets[2]);
                    octet3 = Int32.Parse(octets[3]);
                }

                //Show a tooltip/error if a octet is not in valid IP address format and update the ipIsValid variable
                catch (IndexOutOfRangeException)
                {
                    ipErrorPictureBox.Visible = true;
                    tcpipToolTip.Hide(ipTextBox);
                    tcpipToolTip.Show(@"Please enter a valid TCP\IP address. (example: 172.16.34.12)", ipTextBox, 5000);
                    Globals.ipIsValid = false;
                }

                catch (FormatException)
                {
                    ipErrorPictureBox.Visible = true;
                    tcpipToolTip.Hide(ipTextBox);
                    tcpipToolTip.Show(@"Please enter a valid TCP\IP address. (example: 172.16.34.12)", ipTextBox, 5000);
                    Globals.ipIsValid = false;
                }

                if (Match)
                {
                    //Show a tooltip/error if the IP address is a reserved address and update the ipIsValid variable
                    if (octet0 + octet1 + octet2 + octet3 == 0 || Value == @"255.255.255.255" || Value == @"127.0.0.1")
                    {
                        ipErrorPictureBox.Visible = true;
                        tcpipToolTip.Hide(ipTextBox);
                        tcpipToolTip.Show(@"Please enter a valid TCP/IP address. Cannot be a reserved address.", ipTextBox, 5000);
                        Globals.ipIsValid = false;
                    }

                    //Remove any tooltip/errors and update the ipIsValid variable
                    else
                    {
                        ipErrorPictureBox.Visible = false;
                        tcpipToolTip.Hide(ipTextBox);
                        Globals.ipIsValid = true;
                    }
                }

                //Show a tooltip/error if the ipTextBox text doesn't match the IP address format
                else
                {
                    ipErrorPictureBox.Visible = true;
                    tcpipToolTip.Hide(ipTextBox);
                    tcpipToolTip.Show(@"Please enter a valid TCP/IP address. (example: 172.16.34.12)", ipTextBox, 5000);
                    Globals.ipIsValid = false;
                }
            }
        }

        private void ipTextBox_TextChanged(object sender, EventArgs e)
        {
            //Remove the tooltip/error when text is changed
            tcpipToolTip.Hide(ipTextBox);
            ipErrorPictureBox.Visible = false;
        }

        private void installMultipleButton_Click(object sender, EventArgs e)
        {
            //TODO
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            this.Close();
            installTypeForm installTypeForm = new installTypeForm();
            installTypeForm.Show();
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            if (existingPortRadioButton.Checked)
            {
                //Show a tooltip/error if no port has been selected
                if (String.IsNullOrEmpty(existingPortComboBox.Text))
                {
                    existingPortErrorPictureBox.Visible = true;
                    existingPortToolTip.Hide(existingPortComboBox);
                    existingPortToolTip.Show(@"Please select a port.", existingPortComboBox, 5000);
                }

                else
                {
                    //Update the systemType variable based on systemTypeCheckBox
                    if (systemTypeCheckBox.Checked)
                    {
                        Globals.systemType = "Both";
                    }

                    else
                    {
                        Globals.systemType = "One";
                    }

                    //Update printerPort and printerName Global variables
                    Globals.printerPort = existingPortComboBox.Text;
                    Globals.printerName = printerNameTextBox.Text;

                    this.Hide();
                    fontSelectionForm fontSelectionForm = new fontSelectionForm();
                    fontSelectionForm.Show();
                }

                //Used to trigger the installPort function
                Globals.createNewPort = false;
            }

            else
            {
                if (Globals.ipIsValid)
                {
                    //Update the systemType variable based on systemTypeCheckBox
                    if (systemTypeCheckBox.Checked)
                    {
                        Globals.systemType = "Both";
                    }

                    else
                    {
                        Globals.systemType = "One";
                    }

                    //Update printerPort and printerName Global variables
                    Globals.printerPort = ipTextBox.Text;
                    Globals.printerName = printerNameTextBox.Text;

                    this.Hide();
                    fontSelectionForm fontSelectionForm = new fontSelectionForm();
                    fontSelectionForm.Show();
                }

                //Show tooltip/error if IP address is not valid and do not move to next form
                else
                {
                    ipErrorPictureBox.Visible = true;
                    tcpipToolTip.Hide(ipTextBox);
                    tcpipToolTip.Show(@"Please enter a valid TCP/IP address. (example: 172.16.34.12)", ipTextBox, 5000);
                }

                //Used to trigger the installPort function
                Globals.createNewPort = true;
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

