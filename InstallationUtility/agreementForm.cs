﻿using System;
using System.Windows.Forms;

//Errors generated from this section will have an Event ID in the range of 200-299

namespace InstallationUtility
{
    public partial class agreementForm : Form
    {
        public agreementForm()
        {
            InitializeComponent();

            acceptRadioButton.CheckedChanged += acceptRadioButton_CheckedChanged;
        }

        private void agreementForm_Load(object sender, EventArgs e)
        {
            versionLabel.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
   

        private void acceptRadioButton_CheckedChanged(Object sender, EventArgs e)
        {
            if (acceptRadioButton.Checked == true)
            {
                nextButton.Enabled = true;
            }
            else
            {
                nextButton.Enabled = false;
            }
        }

        private void nextButton_Click(object sender, EventArgs e)
        {
            Functions.deleteFonts();
            this.Close();
            installTypeForm installTypeForm = new installTypeForm();
            installTypeForm.Show();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
